package com.example.adalligo.crypto.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.api.AdalligoAPI;
import com.example.adalligo.crypto.helper.Validation;

import org.json.JSONException;

public class Register extends AppCompatActivity {

    // Fields
    Context thisActivity;
    TextView tvJoinUs;
    EditText etUsername;
    EditText etEmail;
    EditText etPwd;
    Button btRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Context
        thisActivity = this;

        // Initializing Views
        tvJoinUs = (TextView) findViewById(R.id.tvWelcome);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPwd = (EditText) findViewById(R.id.etPwd);
        btRegister = (Button) findViewById(R.id.btRegister);

        // Listener for register button that launches login sequence
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // If the username string is left empty
                if (!Validation.validateFields(etUsername.getText().toString())){
                    Toast.makeText(thisActivity, "The username field cannot be left empty", Toast.LENGTH_SHORT).show();

                }

                // If the email address does not match an email string pattern
                else if (!Validation.validateEmail(etEmail.getText().toString())){
                    Toast.makeText(thisActivity, "Invalid Email Address", Toast.LENGTH_SHORT).show();

                }

                // If the password field is left empty
                else if(!Validation.validateFields(etPwd.getText().toString())){
                    Toast.makeText(thisActivity, "The password field cannot be left empty", Toast.LENGTH_SHORT).show();

                }
                // If the username field is not empty, the string in the email filed matches an email pattern, AND the password field is not empty
                else if (Validation.validateFields(etUsername.getText().toString()) && Validation.validateEmail(etEmail.getText().toString()) && Validation.validateFields(etPwd.getText().toString())){
                    // Launch the register sequence
                    try {
                        loginSequence(thisActivity, etUsername.getText().toString(), etEmail.getText().toString(), etPwd.getText().toString());
                    } catch (JSONException e) {
                        Toast.makeText(thisActivity, "There was a network error", Toast.LENGTH_SHORT).show();
                    }
                }

                // Else
                else {
                    Toast.makeText(thisActivity, "Please review all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void loginSequence(Context context, String username, String email, String pwd) throws JSONException {
        AdalligoAPI.asyncRegister(context, username, email, pwd);

    }
}
