package com.example.adalligo.crypto.custom;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Nico on 7/20/2017.
 */

public class CoinPriceScroll {

    // Fields
    private String coinType;
    private double coinOpenPrice;
    private double coinCurrentPrice;
    private double dollarChange;
    private double percentChange;
    private NumberFormat twoDecFormatter;


    public CoinPriceScroll(String coinType){

        twoDecFormatter = new DecimalFormat("#0.00");

        this.coinType = coinType;
        this.coinOpenPrice = 0;
        this.coinCurrentPrice = 0;
        this.dollarChange = 0;
        this.percentChange = 0;


    }

    private void calculateChange(){

        // Calculate dollar change
        this.dollarChange = this.coinCurrentPrice - this.coinOpenPrice;

        // Calculate percent change
        this.percentChange = (this.dollarChange / this.coinOpenPrice) * 100;

    }


    // Methods
    public String getFormattedPrice(){

        calculateChange();

        // Prefix for the price to tell whether positive or negative
        String indicator;

        // If the current price is greater than the opening price (Gain)
        if (coinCurrentPrice > coinOpenPrice){
            indicator = "+";
        }

        // If the current price is less than the opening price (Loss )
        else if (coinCurrentPrice < coinOpenPrice){
            indicator = "-";
        }

        // If the current price is the same as the opening price (No gain or loss)
        else {
            indicator = "";
        }

        // Return formatted string, example: BTC $4,000 | +300.20 (+20.01%)
        return coinType + " $" + twoDecFormatter.format(coinCurrentPrice) + " | " + indicator + twoDecFormatter.format(dollarChange) + " (" + indicator +  twoDecFormatter.format(percentChange) + "%)";
    }


    // Returns coin type
    public String getCoinType(){
        return coinType;
    }

    // Returns dollar change
    public double getDollarChange(){ return dollarChange; }


    // Sets new coin price
    public void setCoinOpenPrice(double openPrice){
        this.coinOpenPrice = openPrice;
    }

    // Sets new coinCurrentPrice
    public void setCoinCurrentPrice(double newCurrentPrice){
        this.coinCurrentPrice = newCurrentPrice;
    }
}
