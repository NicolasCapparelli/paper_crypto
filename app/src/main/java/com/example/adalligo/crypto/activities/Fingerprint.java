package com.example.adalligo.crypto.activities;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.support.v4.os.CancellationSignal;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.api.AdalligoAPI;
import com.example.adalligo.crypto.custom.FingerprintHandler;

public class Fingerprint extends AppCompatActivity implements FingerprintHandler.OnAuthenticationSuccessListener, FingerprintHandler.OnAuthenticationFailureListener {

    // Fields
    private Context thisActivity;
    private KeyguardManager keyguardManager;
    private FingerprintManager fingMang;
    private CancellationSignal cancellationSignal;
    private ImageView ivFingerprintAnimation;
    private TextView tvTitle;
    private TextView tvDirections;
    private Button btUsePwd;
    private SharedPreferences loginPrefs;
    private FingerprintHandler fHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint);

        // TODO Animate fingerprint image and prettify

        // This view needs to be instantiated before the
        tvDirections = (TextView) findViewById(R.id.tvDirections);

        // Context
        thisActivity = this;
        // SharedPreferences
        loginPrefs = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        // Keyguard Manager for lock screen security condition
        keyguardManager = (KeyguardManager) thisActivity.getSystemService(KEYGUARD_SERVICE);
        // Our custom fingerprints
        fHandler = new FingerprintHandler(thisActivity, R.id.tvDirections);
        // Setting the handlers
        fHandler.setOnAuthenticationSucceededListener(this);
        fHandler.setOnAuthenticationFailedListener(this);

        // Checks that the user is running a compatible version of android that supports fingerprint log in (M is for Marshmallow (API Level 23))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            initActivity(thisActivity);
        }

        // If the user's android version is not high enough
        else {


        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        // If the key handler has been instantiated
        if (fHandler != null){
            fHandler.startListening();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        // If the key handler has been instantiated
        if (fHandler != null){
            fHandler.startListening();
        }
    }

    // Initializes the activity
    public void initActivity(Context context){

        // Fingerprint Manager
        fingMang = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        // Initializing views
        ivFingerprintAnimation = (ImageView) findViewById(R.id.ivFingerprintAnimation);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        btUsePwd = (Button) findViewById(R.id.btUsePwd);

        // Response code from the handler
        int respCode = fHandler.isFingerprintUsableCode();


        switch (respCode){

            // If no permissions
            case 901:
                // Gets string from string resources that tells user to enable fingerprint permissions and sets the directions TextView to that string
                tvDirections.setText(getResources().getText(R.string.fingerprintIfNoPerms));
                break;

            // If no fingerprint hardware
            case 902:
                // Gets string from string resources that tells user that their device does not support fingerprint log in and sets the directions TextView to that string
                tvDirections.setText(getResources().getText(R.string.fingerprintIfNoSensor));
                break;

            // If no enrolled fingerprints
            case 903:
                // Gets string from string resources that tells user that they do not have any fingerprints enrolled and sets the directions TextView to that string
                tvDirections.setText(getResources().getText(R.string.fingerprintIfNoEnroll));
                break;

            // If no locks-screen security
            case 904:
                // Gets string from string resources that tells user that they do not have a Lock Screen Security and sets the directions TextView to that string
                tvDirections.setText(getResources().getText(R.string.fingerprintIfNoSecurity));
                break;

            // If successful
            case 900:
                tvDirections.setText(getResources().getString(R.string.fingerprintInstructions));
                fHandler.startListening();
                break;

            // Default
            default:
                tvDirections.setText("Put");
        }

        // Listener for password button
        btUsePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Send the user to login activity
                Intent toLogin = new Intent(thisActivity, Login.class);
                startActivity(toLogin);
            }
        });
    }

    // Implementing Fingerprint handler interfaces

    // On success
    @Override
    public void onAuthSucceeded() throws Exception {
        fHandler.stopListening();
        AdalligoAPI.asyncLogin(thisActivity, loginPrefs.getString("email", ""), loginPrefs.getString("pwd", ""));
    }

    // On failure
    @Override
    public void onAuthFailed() {
        tvDirections.setText(thisActivity.getResources().getString(R.string.fingerprintAuthFailed));
        fHandler.startListening();
    }
}
