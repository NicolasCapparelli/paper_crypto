package com.example.adalligo.crypto.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.adapters.RssFeedListAdapter;
import com.example.adalligo.crypto.constants.RssFeedModel;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.R.attr.name;

/**
 * Created by andrew.kenreich on 7/7/2017.
 */

public class RssFeed extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private RecyclerView mRecyclerView;
    private EditText mEditText;
    private Button mFetchFeedButton;
    private SwipeRefreshLayout mSwipeLayout;
    private TextView mFeedTitleTextView;
    private TextView mFeedLinkTextView;
    private TextView mFeedDescriptionTextView;
    private TextView mFeedPubDateTextView;

    private List<RssFeedModel> mFeedModelList;
    private String mFeedTitle;
    private String mFeedLink;
    private String mFeedDescription;
    private String mFeedPubDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rssfeed);


        //assigning all the views
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mEditText = (EditText) findViewById(R.id.rssFeedEditText);
        mFetchFeedButton = (Button) findViewById(R.id.fetchFeedButton);
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //setting the feed button to work to get the data
        mFetchFeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FetchFeedTask().execute((Void) null);
            }
        });

        //setting the swipe listener for pulling down and refreshing the data
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchFeedTask().execute((Void) null);
            }
        });
    }

    public List<RssFeedModel> parseFeed(InputStream inputStream) throws XmlPullParserException, IOException {

        //setting the string for our fields
        String title = null;
        String link = null;
        String description = null;
        String pubDate = null;
        boolean isItem = false;

        //setting up array
        List<RssFeedModel> items = new ArrayList<>();


        //XML parsing below
        try {
            //setting up the parser
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(inputStream, null);

            xmlPullParser.nextTag();
            //makes it go to end of doc
            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                int eventType = xmlPullParser.getEventType();

                //tries to get the name of the rss feed
                String name = xmlPullParser.getName();
                if(name == null)
                    continue;

                //read each XML tag in the RSS feed until we get to the end of the document
                //this sets the item to false if its not an item in the RSS Feed
                if(eventType == XmlPullParser.END_TAG) {
                    if(name.equalsIgnoreCase("item")) {
                        isItem = false;
                    }
                    continue;
                }
                //sets item to true so we know we have a item from the rss feed
                if (eventType == XmlPullParser.START_TAG) {
                    if(name.equalsIgnoreCase("item")) {
                        isItem = true;
                        continue;
                    }
                }

                //printing to see that its working
                //Log.d("RssFeed", "Parsing name ==> " + name);
                String result = "";
                //get text and set it to the result
                if (xmlPullParser.next() == XmlPullParser.TEXT) {
                    result = xmlPullParser.getText();
                    //call the next tag after
                    xmlPullParser.nextTag();
                }

                //finding each value from the rss feed based on case value - PubDate might need to change depending on RSS feed
                if (name.equalsIgnoreCase("title")) {
                    title = result;
                } else if (name.equalsIgnoreCase("link")) {
                    link = result;
                } else if (name.equalsIgnoreCase("description")) {
                    description = result;
                } else if (name.equalsIgnoreCase("pubDate")) {
                    pubDate = result;
                }

                //adding item to the list if all are there.
                if (title != null && link != null && description != null&& pubDate != null) {
                    if(isItem) {
                        RssFeedModel item = new RssFeedModel(title, link, description, pubDate);
                        items.add(item);
                    }

                    else {
                        //extra shit, just ignore this.
                        mFeedTitle = title;
                        mFeedLink = link;
                        mFeedDescription = description;
                        mFeedPubDate = pubDate;
                    }

                    //reset these too null for the next pass through and item to false
                    title = null;
                    link = null;
                    description = null;
                    isItem = false;
                }
            }

            //return the item
            return items;
        } finally {
            //close input stream
            inputStream.close();
        }
    }

    //async class to get the feed
    private class FetchFeedTask extends AsyncTask<Void, Void, Boolean> {

        //gets this from the line - have it hardcoded to one atm.
        private String urlLink;

        @Override
        protected void onPreExecute() {
            //show the refreshing icon
            mSwipeLayout.setRefreshing(true);
            //getting the url link from the text input line
            urlLink = mEditText.getText().toString();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (TextUtils.isEmpty(urlLink))
                return false;

            try {
                //checking that its http and not https - could have user put in their own link here.
                if(!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                    urlLink = "http://" + urlLink;

                URL url = new URL(urlLink);
                InputStream inputStream = url.openConnection().getInputStream();
                mFeedModelList = parseFeed(inputStream);
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Error", e);
            } catch (XmlPullParserException e) {
                Log.e(TAG, "Error", e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            mSwipeLayout.setRefreshing(false);

            if (success) {
                // Fill RecyclerView
                mRecyclerView.setAdapter(new RssFeedListAdapter(mFeedModelList));
            } else {
                // Display this if they didnt put in a valid url
                Toast.makeText(RssFeed.this,
                        "Enter a valid Rss feed url",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
