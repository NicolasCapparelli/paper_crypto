package com.example.adalligo.crypto.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.custom.CoinPriceScroll;


import java.util.ArrayList;

/**
 *
 * Created by Nico on 7/20/2017.
 */

public class CoinPriceScrollAdapter extends RecyclerView.Adapter<CoinPriceScrollAdapter.CoinPriceHolder> {

    // Fields
    private ArrayList<CoinPriceScroll> viewableDataList;
    private LayoutInflater inflater;
    private Context context;


    // Constructor
    public CoinPriceScrollAdapter(Context context){
        this.inflater = LayoutInflater.from(context);
        this.viewableDataList = new ArrayList<>();
        this.context = context;

    }

    @Override
    public CoinPriceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // The view that needs to be inflated
        View mainView = inflater.inflate(R.layout.recview_coin_price_scroll, parent, false);

        // Return new Position view holder
        return new CoinPriceHolder(mainView);
    }

    @Override
    public void onBindViewHolder(CoinPriceHolder holder, int position) {

        holder.tvCoinData.setText(viewableDataList.get(position).getFormattedPrice());

        // If dollar change is greater than 0 change text color to green
        if (viewableDataList.get(position).getDollarChange() > 0) {

            holder.tvCoinData.setTextColor(ContextCompat.getColor(this.context, R.color.upwardGreen));
        }

        // If dollar change is less han 0 change text color to red
        else if (viewableDataList.get(position).getDollarChange() < 0) {
            holder.tvCoinData.setTextColor(ContextCompat.getColor(this.context, R.color.downwardRed));
        }

        // If not just leave the text by itself
    }


    @Override
    public int getItemCount() {
        return this.viewableDataList.size();
    }

    // Returns an int of the position in which the coin passed through is located in the viewable data list
    public int getCoinListPosition(String coinType){

        int coinPosition = -1;

        // Iterate through viewableDataList and find where the coinType is located
        for (int i =0; i < this.viewableDataList.size(); i++){

            // If the iterated CoinPriceScroll's coinType matches that of the one passed through
            if (viewableDataList.get(i).getCoinType().equals(coinType)){

               // Set coin position to index of Coin Type
                coinPosition = i;

                // Stop iterating
                break;
            }
        }
        return coinPosition;

    }

    // Returns position of CoinPriceScroll object with the coin
    public int getCoinPosition(String coin){

        // Iterate through every position in viewable list
        for (int i = 0; i < this.viewableDataList.size(); i++) {

            // if the objects coin type is equivalent of the one passed in
            if (this.viewableDataList.get(i).getCoinType().equals(coin)){
                // return the objects position
                return i;
            }
        }

        // If the for loop doesn't find anything, return -1
        return -1;
    }

    public void setViewableDataList(ArrayList<CoinPriceScroll> newData){
        this.viewableDataList = newData;
    }

    public void setSpecificData(Bundle data, int position){
        CoinPriceScroll coinToUpdate = this.viewableDataList.get(position);

        coinToUpdate.setCoinCurrentPrice(data.getDouble("currentPrice"));
        coinToUpdate.setCoinOpenPrice(data.getDouble("openPrice"));

    }

    public class CoinPriceHolder extends RecyclerView.ViewHolder {

        // Fields
        LinearLayout ltContainer;
        TextView tvCoinData;

        public CoinPriceHolder(View itemView) {
            super(itemView);

            ltContainer = (LinearLayout) itemView.findViewById(R.id.ltCoinPriceContainer);
            tvCoinData = (TextView) itemView.findViewById(R.id.tvCoinData);

        }
    }
}
