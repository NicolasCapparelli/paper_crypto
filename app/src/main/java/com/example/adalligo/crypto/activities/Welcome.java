package com.example.adalligo.crypto.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.adapters.ChartAdapter;

public class Welcome extends FragmentActivity {

    // Fields
    Context thisActivity;
    TextView tvWelcome;
    ImageView ivWelcomeLogo;
    TextView tvWelcomeDescription;
    LinearLayout ltButton;
    Button btRegister;
    Button btLogin;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // Context
        thisActivity = this;






        SharedPreferences loginPrefs = getSharedPreferences("loginPrefs", MODE_PRIVATE);

        // Booleans to check if the user is logged in and if their auth token is expired
        boolean isLoggedIn = loginPrefs.getBoolean("isLoggedIn", false);
        boolean isTokenExpired = loginPrefs.getBoolean("isTokenExpired", true);

        // If the user is logged in and their token isn't expired
        if (isLoggedIn && !isTokenExpired){

            // Go to home activity
            Intent toHome = new Intent(thisActivity, Home.class);
            startActivity(toHome);
        }

        // If the user has previously logged in but their token is expired
        else if (isLoggedIn && isTokenExpired){

            // Take the user to fingerprint activity
            Intent toFingerprint = new Intent(thisActivity, Fingerprint.class);
            startActivity(toFingerprint);
        }

        // If the user has never signed i
        else {
            // Context
            thisActivity = this;

            // Initializing Views
            tvWelcome = (TextView) findViewById(R.id.tvWelcome);
            ivWelcomeLogo = (ImageView) findViewById(R.id.ivWelcomeLogo);
            tvWelcomeDescription = (TextView) findViewById(R.id.tvWelcomeDescription);
            ltButton = (LinearLayout) findViewById(R.id.ltButton);
            btRegister = (Button) findViewById(R.id.btRegister);
            btLogin = (Button) findViewById(R.id.btLogin);


            // Click Listeners for buttons

            // On click, go to register activity
            btRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toRegister = new Intent(thisActivity, Register.class);
                    startActivity(toRegister);
                }
            });

            // On click, go to login activity
            btLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toLogin = new Intent(thisActivity, Login.class);
                    startActivity(toLogin);
                }
            });
        }

    }






}
