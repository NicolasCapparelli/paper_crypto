package com.example.adalligo.crypto.helper;

import java.util.ArrayList;

/**
 * Created by Nico on 6/24/2017.
 */

public class FinanceCalculations {

    // Returns the average cost for a security given the number of shares and different costs
    /*
    @Param coins: The number of total coins the user owns
    @Param buyPrices: The different prices at which the user bought the shares
    */
    public static Double averageCost(ArrayList<Double> buyPrices){

        // Initializing sum (sum of the array)
        Double sum = 0.0;

        // Iterating through array
        for (int i = 0; i < buyPrices.size(); i++){

            // Adding every number in the array and putting that value into sum
            sum = sum + buyPrices.get(i);
            System.out.println(sum);

        }

        // Calculating average by dividing the sum by the amount of positions in the array and returning the value
        return sum/buyPrices.size();

    }




}
