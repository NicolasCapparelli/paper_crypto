package com.example.adalligo.crypto.constants;

/**
 * Created by andrew.kenreich on 7/7/2017.
 */

public class RssFeedModel {

    public String title;
    public String link;
    public String description;
    public String pubDate;

    public RssFeedModel(String title, String link, String description, String pubDate) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.pubDate = pubDate;
    }
}
