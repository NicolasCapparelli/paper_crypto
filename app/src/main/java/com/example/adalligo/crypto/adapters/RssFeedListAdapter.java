package com.example.adalligo.crypto.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.constants.RssFeedModel;

import java.util.List;

/**
 * Created by andrew.kenreich on 7/7/2017.
 */

public class RssFeedListAdapter extends RecyclerView.Adapter<RssFeedListAdapter.FeedModelViewHolder> {

    // Fields
    private List<RssFeedModel> mRssFeedModels;

    public static class FeedModelViewHolder extends RecyclerView.ViewHolder {
        private View rssFeedView;

        public FeedModelViewHolder(View v) {
            super(v);
            rssFeedView = v;
        }
    }

    public RssFeedListAdapter(List<RssFeedModel> rssFeedModels) {
        mRssFeedModels = rssFeedModels;
    }

    @Override
    public FeedModelViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        //inflating the view holder v
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rss_feed, parent, false);

        //set holder
        FeedModelViewHolder holder = new FeedModelViewHolder(v);

        //return the holder
        return holder;
    }
    // Binds (Matches up) the data class with the view
    @Override
    public void onBindViewHolder(FeedModelViewHolder holder, int position) {

        // Getting the Data class from the data rssfeedmodel to be used for X view holder
        final RssFeedModel rssFeedModel = mRssFeedModels.get(position);

        // Setting the values for the views inside the view holder with the data from the above RssFeedModel that corresponds to this view's position
        // rssFeedModel is set from the constants - view id's rom item rssfeed
        ((TextView)holder.rssFeedView.findViewById(R.id.titleText)).setText(rssFeedModel.title);
        ((TextView)holder.rssFeedView.findViewById(R.id.descriptionText)).setText(rssFeedModel.description);
        ((TextView)holder.rssFeedView.findViewById(R.id.linkText)).setText(rssFeedModel.link);
        ((TextView)holder.rssFeedView.findViewById(R.id.dateText)).setText(rssFeedModel.pubDate);
    }

    @Override
    // gets the count of how many rss views are in then model
    public int getItemCount() {
        return mRssFeedModels.size();
    }
}