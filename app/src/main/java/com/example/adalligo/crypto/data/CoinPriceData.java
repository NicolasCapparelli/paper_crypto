package com.example.adalligo.crypto.data;

import com.example.adalligo.crypto.api.AdalligoAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Nico on 7/9/2017.
 *
 * The entire point of this class is to have a local central location from which to pull prices from,
 * saving a bunch of API Calls to our server 😏
 */

public class CoinPriceData {


    // Fields
    public double btcPrice;
    public double ethPrice;
    public double ltcPrice;
    private onBitcoinPriceChangeListener bitcoinPriceListener;
    private onEthereumPriceChangeListener etherumPriceListener;
    private onLitecoinPriceChangeListener litecoinPriceListener;

    // Interface for BTC price change
    public interface onBitcoinPriceChangeListener {
        void onBtcPriceChange();
    }

    // Interface for ETH price change
    public interface onEthereumPriceChangeListener{
        void onEthPriceChange();
    }

    // Interface for LTC price change
    public interface onLitecoinPriceChangeListener{
        void onLtcPriceChange();
    }


    // Setters for the interfaces
    public void setOnBitcoinPriceChangeListener(onBitcoinPriceChangeListener listener){
        bitcoinPriceListener = listener;
    }

    public void setOnEtherumPriceChangeListener(onEthereumPriceChangeListener listener){
        etherumPriceListener = listener;
    }

    public void setOnLitecoinPriceChangeListener(onLitecoinPriceChangeListener listener){
        litecoinPriceListener = listener;
    }


    // Constructor
    public CoinPriceData(){

        // Try setting prices from the database
        try {
            asyncGetBTCPrice();
            asyncGetETHPrice();
            asyncGetLTCPrice();

        }

        // If above fails set prices to 0
        catch (Exception e) {
            this.btcPrice = 1.00;
            this.ethPrice = 1.00;
            this.ltcPrice= 1.00;
        }

    }

    // Get BTC Prices
    public void asyncGetBTCPrice() throws IOException, JSONException {

        Request request = AdalligoAPI.getCoinPrice("btc");

        AdalligoAPI.CLIENT.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                JSONObject respObject = null;
                // Try putting response in JSONObject
                try {
                    respObject = new JSONObject(response.body().string());
                    // Parsing response
                    btcPrice = respObject.getDouble("price");
                }

                catch (JSONException e) {
                    e.printStackTrace();
                }
                // Firing interface
                bitcoinPriceListener.onBtcPriceChange();
            }
        });
    }

    // Get Etherum price
    public void asyncGetETHPrice() throws IOException, JSONException {

        Request request = AdalligoAPI.getCoinPrice("eth");

        AdalligoAPI.CLIENT.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                JSONObject respObject = null;
                // Try putting response in JSONObject
                try {
                    respObject = new JSONObject(response.body().string());
                    // Parsing response
                    ethPrice = respObject.getDouble("price");
                }

                catch (JSONException e) {
                    e.printStackTrace();
                }
                // Firing interface
                etherumPriceListener.onEthPriceChange();
            }
        });
    }

    // Get Etherum price
    public void asyncGetLTCPrice() throws IOException, JSONException {

        Request request = AdalligoAPI.getCoinPrice("ltc");

        AdalligoAPI.CLIENT.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                JSONObject respObject = null;
                // Try putting response in JSONObject
                try {
                    respObject = new JSONObject(response.body().string());
                    // Parsing response
                    ltcPrice = respObject.getDouble("price");
                }

                catch (JSONException e) {
                    e.printStackTrace();
                }
                // Firing interface
                litecoinPriceListener.onLtcPriceChange();
            }
        });
    }


/// Getters and Setters ///

    // Returns Btc Price
    public double getBtcPrice(){
        return btcPrice;
    }

    // Returns Eth Price
    public double getEthPrice(){
        return ethPrice;
    }

    // Returns Ltc price
    public double getLtcPrice(){
        return ltcPrice;
    }

    // btcPrice as String
    public String getBtcPriceString(){
        return String.valueOf(btcPrice);
    }

    // ethPrice as String
    public String getEthPriceString(){
        return String.valueOf(ethPrice);
    }

    // ltcPrice as String
    public String getLtcPriceString(){
        return String.valueOf(ethPrice);
    }


}
