package com.example.adalligo.crypto.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adalligo.crypto.R;

import junit.framework.Test;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.R.id.button1;
import static android.R.id.button3;
import static android.R.id.edit;
import static android.icu.util.Currency.CurrencyUsage.CASH;
import static com.example.adalligo.crypto.R.id.amountofshares;
import static com.example.adalligo.crypto.R.id.costoftrade;
import static com.example.adalligo.crypto.R.id.price;
import static com.example.adalligo.crypto.R.layout.mathtest;
import static com.example.adalligo.crypto.R.string.pwd;
import static com.example.adalligo.crypto.R.string.username;
import static java.lang.Long.parseLong;

/**
 * Created by andrew.kenreich on 6/24/2017.
 */

public class MathTesting extends AppCompatActivity {

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    Button BtPlaceOrder;
    Button BtChart;
    public static String bid = "";
    public static String ask = "";
    public static String price = "";
    public static String time = "";
    public static String btcurl = "";
    public static String email = "";
    public static String token = "";
    public static String ethstr = "";
    public static String btcstr = "";
    public static String ltcstr = "";
    public static String cashstr = "";
    public static String currentcoin = "";
    public static String currenttype = "";
    public static double sharesamt = 0;
    private Spinner spinner1;
    private Spinner buyspinner;
    private static Context thisActivity;
    private ImageView coinsvg;




    //set balance and bal of coins
    public static double balance = 0;
    public static double ethbalance = 0;
    public static double ltcbalance = 0;
    public static double btcbalance = 0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mathtest);
        // Context
        thisActivity = this;

        //get shared pref email and then call GetBalance to get the balance of user.
        SharedPreferences loginPrefs = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        email = loginPrefs.getString("email", "email not found");
        token = loginPrefs.getString("userToken", "token not found");
        GetBalance();


        //Listener for button chart to go to page.
        //BtChart = (Button) findViewById(R.id.chart);
        //BtChart.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        startActivity(new Intent(MathTesting.this, TradingViewChart.class));
        //    }
        //});




        //setting up the dropdown for buying/selling
        buyspinner = (Spinner)findViewById(R.id.ordertype);
        final String[]buytypes = {"Buy to Open", "Sell to Close"}; // - To add later "Sell to Open", "Buy to Close"};

        //declaring array and adding the items to it from the array "coins" string above
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(MathTesting.this,
                R.layout.spinneritems,buytypes);

        //layout to use - simple spinner dropdown
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //set the adapter
        buyspinner.setAdapter(adapter2);
        currenttype = "Buy to Open";
        //set on item listener
        buyspinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //case positions for 0,1,2
                switch (position) {
                    case 0:
                        //make a toast to see what spinner selected
                        Toast.makeText(parent.getContext(), "Buy to Open!", Toast.LENGTH_SHORT).show();
                        currenttype = "Buy to Open";
                        //String test = (String) parent.getItemAtPosition(position);
                        //Log.d("test",test);
                        //calls the OkHTTP method and pass the ticker to the method
                        break;
                    case 1:
                        Toast.makeText(parent.getContext(), "Sell to Close!", Toast.LENGTH_SHORT).show();
                        currenttype = "Sell to Close";
                        break;
                    //case 2:
                    //    Toast.makeText(parent.getContext(), "Sell to Open!", Toast.LENGTH_SHORT).show();
                    //    currenttype = "Sell to Open";
                    //    break;
                    //case 3:
                    //    currenttype = "Buy to Close";
                    //    break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                currenttype = "Buy to Open";
            }
        });





        //setting up the dropdown spinner and what to do if one gets selected for coins.

        spinner1 = (Spinner)findViewById(R.id.coinspinner);
        final String[]coins = {"BTC", "ETH", "LTC"};

        //declaring array and adding the items to it from the array "coins" string above
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MathTesting.this,
                R.layout.spinneritems,coins);

        //layout to use - simple spinner dropdown
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //set the adapter
        spinner1.setAdapter(adapter);
        currentcoin = "BTC";

        //getting the imageview id to put the coin svg in
        coinsvg =(ImageView)findViewById(R.id.orderSVG);
        //set on item listener
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //case positions for 0,1,2
                switch (position) {
                    case 0:
                        //make a toast to see what spinner selected
                        Toast.makeText(parent.getContext(), "Loading BTC!", Toast.LENGTH_SHORT).show();
                        currentcoin = "BTC";
                        //String test = (String) parent.getItemAtPosition(position);
                        //Log.d("test",test);
                        btcurl = "http://104.131.0.17:8081/api/p1/price/btc";
                        //getting the drawable for the imageview and setting it
                        Drawable btc = ContextCompat.getDrawable(thisActivity, R.drawable.ic_ad_btc_logo);
                        coinsvg.setImageDrawable(btc);

                        //calls the OkHTTP method and pass the ticker to the method
                        GetCoins();
                        break;
                    case 1:
                        Toast.makeText(parent.getContext(), "Loading ETH!", Toast.LENGTH_SHORT).show();
                        currentcoin = "ETH";
                        btcurl = "http://104.131.0.17:8081/api/p1/price/eth";
                        //getting the drawable for the imageview and setting it
                        Drawable eth = ContextCompat.getDrawable(thisActivity, R.drawable.ic_ad_eth_logo);
                        coinsvg.setImageDrawable(eth);
                        //calls the OkHTTP method and pass the ticker to the method
                        GetCoins();
                        break;
                    case 2:
                        Toast.makeText(parent.getContext(), "Loading LTC!", Toast.LENGTH_SHORT).show();
                        currentcoin = "LTC";
                        btcurl = "http://104.131.0.17:8081/api/p1/price/ltc";
                        //getting the drawable for the imageview and setting it
                        Drawable ltc = ContextCompat.getDrawable(thisActivity, R.drawable.ic_ad_ltc_logo);
                        coinsvg.setImageDrawable(ltc);
                        //calls the OkHTTP method and pass the ticker to the method
                        GetCoins();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                currentcoin = "BTC";
                btcurl = "http://104.131.0.17:8081/api/p1/price/btc";
                Drawable btc = ContextCompat.getDrawable(thisActivity, R.drawable.ic_ad_btc_logo);
                coinsvg.setImageDrawable(btc);
            }
        });







                final EditText amtshares = (EditText) findViewById(R.id.amountofshares);


                //this will listen to the textbox and update the cost of trade
                amtshares.addTextChangedListener(new TextWatcher() {

                    public void afterTextChanged(Editable s) {}

                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {

                        UpdatePrice();
                    }
                    //code goes here for when text changes
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {

                        UpdatePrice();

                    }
                });


                //Listener for button buy
                BtPlaceOrder = (Button) findViewById(R.id.placeorder);

                BtPlaceOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final TextView edit = (TextView) findViewById(R.id.price);
                        final EditText shares = (EditText) findViewById(amountofshares);
                        final TextView costoftrade = (TextView) findViewById(R.id.costoftrade);

                        //convert to string and then to float for big numbas
                        String shs = shares.getText().toString();
                        String cost = costoftrade.getText().toString();
                        double costtrade = Double.parseDouble(cost);
                        sharesamt = Double.parseDouble(shs);
                        //Log.d("test", price);
                        //checking to see if the user has enough money to make the purchase and what they have selected on the dropdown.
                        if(balance > costtrade && currenttype=="Buy to Open") {
                            //handler to add the share amount depended on what coin is selected and update it.
                            if (currentcoin == "BTC") {
                                CalcPriceBuy();
                            }
                            if (currentcoin == "LTC") {
                                ltcbalance = ltcbalance + sharesamt;
                                CalcPriceBuy();
                            }
                            if (currentcoin == "ETH") {
                                ethbalance = ethbalance + sharesamt;
                                CalcPriceBuy();
                            }

                            //checking to see if the user has enough money to make the purchase and what they have selected on the dropdown. - Sell portion
                        }else if (currenttype=="Sell to Close") {
                            //small handler for now to stop someone from going negative buying.
                            //handler to add the share amount depended on what coin is selected and update it.
                            if (currentcoin == "BTC" && sharesamt <= btcbalance) {
                                btcbalance = btcbalance - sharesamt;
                                CalcPriceSell();
                            }else if (currentcoin == "BTC"){
                                Toast.makeText(thisActivity, "Not Enough Shares to sell, you only have " + btcbalance + " BTC", Toast.LENGTH_SHORT).show();
                            }
                            if (currentcoin == "LTC" && sharesamt <= ltcbalance) {
                                ltcbalance = ltcbalance - sharesamt;
                                CalcPriceSell();
                            }else if (currentcoin == "LTC"){
                                Toast.makeText(thisActivity, "Not Enough Shares to sell, you only have " + ltcbalance + " LTC", Toast.LENGTH_SHORT).show();
                            }
                            if (currentcoin == "ETH" && sharesamt <= ethbalance) {
                                ethbalance = ethbalance - sharesamt;
                                CalcPriceSell();
                            }else if (currentcoin == "ETH"){
                                Toast.makeText(thisActivity, "Not Enough Shares to sell, you only have " + ethbalance + " ETH", Toast.LENGTH_SHORT).show();
                            }


                        }else{

                            Toast.makeText(thisActivity, "Not Enough Money to Buy Shares", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }

    // this holds the methods to send the balance on Buy to the API and update the UI. - Easier to add it here then add up top on the checks.
    public void CalcPriceBuy(){
        final TextView edit = (TextView) findViewById(R.id.price);
        final EditText bal = (EditText) findViewById(R.id.balance);
        final EditText shares = (EditText) findViewById(amountofshares);

        //convert to string and then to float for big numbas
        String a = edit.getText().toString();
        String shs = shares.getText().toString();
        double price = Double.parseDouble(a);
        sharesamt = Double.parseDouble(shs);
        //adding the numbers
        balance = balance - (price * sharesamt);
        //converting back

        //set textbox to the bal
        NumberFormat decimal = new DecimalFormat("#,###.00");
        String balanceformatted = decimal.format(balance);
        bal.setText(balanceformatted);
        //send the updated balance of the coin
        UpdateBalanceCoinsBuy();
        //send the updated balance for the CASH too...Maybe later need to do these in the same API call...but works for now.
        UpdateBalanceCash();
        sharesamt = 0;
    }
    // this holds the methods to send the balance on Sell to the API and update the UI. - Easier to add it here then add up top on the checks.
    public void CalcPriceSell(){
        final TextView edit = (TextView) findViewById(R.id.price);
        final EditText bal = (EditText) findViewById(R.id.balance);
        final EditText shares = (EditText) findViewById(amountofshares);

        //convert to string and then to float for big numbas
        String a = edit.getText().toString();
        String shs = shares.getText().toString();
        double price = Double.parseDouble(a);
        sharesamt = Double.parseDouble(shs);
        //adding the numbers
        balance = (price * sharesamt) + balance;
        //converting back

        //set textbox to the bal
        NumberFormat decimal = new DecimalFormat("#,###.00");
        String balanceformatted = decimal.format(balance);
        bal.setText(balanceformatted);
        //send the updated balance of the coin
        UpdateBalanceCoinsSell();
        //send the updated balance for the CASH too...Maybe later need to do these in the same API call...but works for now.
        UpdateBalanceCash();
        sharesamt = 0;
    }
    //call this to update the prices on change of anything in screen.
    public void UpdatePrice(){
        final EditText amtshares = (EditText) findViewById(R.id.amountofshares);
        final TextView costoftrade = (TextView) findViewById(R.id.costoftrade);
        try {
            String shs = amtshares.getText().toString();
            final TextView edit = (TextView) findViewById(R.id.price);
            String a = edit.getText().toString();
            double price = Double.parseDouble(a);
            sharesamt = Double.parseDouble(shs);

            //getting the cost of trade
            double cost = price * sharesamt;
            //converting back
            String cost1 = String.valueOf(cost);


            //set textbox to the bal
            costoftrade.setText(cost1);
        } catch (NumberFormatException e) {
            //catches if its empty on the string
            costoftrade.setText("0");
        }
    }

    public void GetCoins() {
        //my first okhttp to get the prices of BTC to put in here :)

        OkHttpClient client = new OkHttpClient();
        //String btcurl = "http://104.131.0.17:8081/api/p1/price/btc";

        Request request = new Request.Builder()
                .url(btcurl)
                .build();

        Log.d("url", btcurl);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        String jsonData = null;
                        try {
                            jsonData = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        JSONObject Jobject = null;
                        try {
                            Jobject = new JSONObject(jsonData);
                            price = Jobject.getString("price");
                            time = Jobject.getString("time");
                            bid = Jobject.getString("bid");
                            ask = Jobject.getString("ask");
                            Log.d("test", price);
                            Log.d("test", time);
                            Log.d("test", bid);
                            Log.d("test", ask);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        TextView bidview = (TextView) findViewById(R.id.bid);
                        TextView askview = (TextView) findViewById(R.id.ask);
                        TextView edit = (TextView) findViewById(R.id.price);

                        bidview.setText(bid);
                        askview.setText(ask);
                        edit.setText(price);


                        //sets the updated price after you get em
                        UpdatePrice();

                    }
                });
            }
        });
        }



    //Get balance for user :)
    public void GetBalance() {


        OkHttpClient client = new OkHttpClient();
        //building balance call
        String btcbal = "http://104.131.0.17:8080/api/v1/users/"+email+"/balance";

        Request request = new Request.Builder()
                .url(btcbal)
                .header("x-access-token", token)
                .build();

        Log.d("url", btcbal);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        String jsonData = null;
                        try {
                            jsonData = response.body().string();
                            Log.d("jsonData", jsonData);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        JSONArray Jarray= null;
                        JSONObject Jobject = null;
                        try {
                            Jobject = new JSONObject(jsonData);
                            //get coinbalance then get each object in coin balance - 0 - 4 and assign them to the object they are
                            JSONArray coinb = Jobject.getJSONArray("coinbalance");
                            JSONObject ethobj = coinb.getJSONObject(0);
                            JSONObject ltcobj = coinb.getJSONObject(1);
                            JSONObject btcobj = coinb.getJSONObject(2);
                            JSONObject cashobj = coinb.getJSONObject(3);

                            //get the string values of the obj's then convert them to doubles
                            ethstr = ethobj.getString("balance");
                            ltcstr = ltcobj.getString("balance");
                            btcstr = btcobj.getString("balance");
                            cashstr = cashobj.getString("balance");

                            ethbalance = Double.parseDouble(ethstr);
                            ltcbalance = Double.parseDouble(ltcstr);
                            btcbalance = Double.parseDouble(btcstr);
                            balance = Double.parseDouble(cashstr);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EditText bidview = (EditText) findViewById(R.id.balance);

                        // Used to format doubles to two decimals
                        NumberFormat decimal = new DecimalFormat("#,###.00");
                        String balanceformatted = decimal.format(balance);
                        bidview.setText(balanceformatted);

                    }
                });
            }
        });
    }

    //update the balance of the coints
    public void UpdateBalanceCoinsBuy() {
        //my first okhttp to get the prices of BTC to put in here :)

        OkHttpClient client = new OkHttpClient();
        String btcupdatebal = "http://104.131.0.17:8080/api/v1/users/"+email+"/updatebalance";


        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        JSONObject json = new JSONObject();
        JSONObject coinJson = new JSONObject();

        //this will add the correct balance when update is called.
        try {
            if (currentcoin == "BTC") {
                //total balance to update and send
                coinJson.put("balance", ltcbalance);
                //ask price that security bought at.
                coinJson.put("coincost", ask);
                //total share size to update and send
                coinJson.put("possize", sharesamt);
            }
            if (currentcoin == "LTC") {
                coinJson.put("balance", ltcbalance);
                coinJson.put("coincost", ask);
                coinJson.put("possize", sharesamt);
            }
            if (currentcoin == "ETH") {
                coinJson.put("balance", ethbalance);
                coinJson.put("coincost", ask);
                coinJson.put("possize", sharesamt);
            }
            //coinJson.put("balance", sharesamt);
            coinJson.put("coin", currentcoin);
            json.put("newCoinbalance",coinJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(json));

        Log.d("json", String.valueOf(json));

        Request request = new Request.Builder()
                .url(btcupdatebal)
                .header("x-access-token", token)
                .put(params)
                .build();

        Log.d("url", btcupdatebal);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        String jsonData = null;
                        try {
                            jsonData = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        JSONObject Jobject = null;
                        Log.d("resp",jsonData);


                        //EditText balance = (EditText) findViewById(R.id.balance);

                        //balance.setText(cashstr);

                    }
                });
            }
        });
    }

    //update the balance of the coins on sell
    public void UpdateBalanceCoinsSell() {
        //my first okhttp to get the prices of BTC to put in here :)

        OkHttpClient client = new OkHttpClient();
        String btcupdatebal = "http://104.131.0.17:8080/api/v1/users/"+email+"/updatebalance";


        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        JSONObject json = new JSONObject();
        JSONObject coinJson = new JSONObject();

        //this will add the correct balance when update is called.
        try {
            if (currentcoin == "BTC") {
                //total balance to update and send
                coinJson.put("balance", btcbalance);
                //bid amount that order sells at
                coinJson.put("coincost", bid);
                //how many shares sold from position
                coinJson.put("possize", sharesamt);
            }
            if (currentcoin == "LTC") {
                coinJson.put("balance", ltcbalance);
                coinJson.put("coincost", bid);
                coinJson.put("possize", sharesamt);
            }
            if (currentcoin == "ETH") {
                coinJson.put("balance", ethbalance);
                coinJson.put("coincost", bid);
                coinJson.put("possize", sharesamt);
            }
            //coinJson.put("balance", sharesamt);
            coinJson.put("coin", currentcoin);
            json.put("newCoinbalance",coinJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(json));

        Log.d("json", String.valueOf(json));

        Request request = new Request.Builder()
                .url(btcupdatebal)
                .header("x-access-token", token)
                .put(params)
                .build();

        Log.d("url", btcupdatebal);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        String jsonData = null;
                        try {
                            jsonData = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        JSONObject Jobject = null;
                        Log.d("resp",jsonData);


                        //EditText balance = (EditText) findViewById(R.id.balance);

                        //balance.setText(cashstr);

                    }
                });
            }
        });
    }
    //update the cash balance
    public void UpdateBalanceCash() {
        //Updates the Cash Balance in user acct

        OkHttpClient client = new OkHttpClient();
        String btcupdatebal = "http://104.131.0.17:8080/api/v1/users/"+email+"/updatebalance";


        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        JSONObject json = new JSONObject();
        JSONObject coinJson = new JSONObject();

        //creating the call
        try {
            coinJson.put("balance", balance);
            coinJson.put("coin", "CASH");
            json.put("newCoinbalance",coinJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(json));

        Log.d("json", String.valueOf(json));

        Request request = new Request.Builder()
                .url(btcupdatebal)
                .header("x-access-token", token)
                .put(params)
                .build();

        Log.d("url", btcupdatebal);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        String jsonData = null;
                        try {
                            jsonData = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        JSONObject Jobject = null;
                        Log.d("resp",jsonData);


                        //EditText balance = (EditText) findViewById(R.id.balance);

                        //balance.setText(cashstr);

                    }
                });
            }
        });
    }
}



