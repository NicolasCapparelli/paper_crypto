package com.example.adalligo.crypto.custom;



import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adalligo.crypto.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.example.adalligo.crypto.activities.MathTesting.btcstr;
import static com.example.adalligo.crypto.activities.MathTesting.cashstr;
import static com.example.adalligo.crypto.activities.MathTesting.ethstr;
import static com.example.adalligo.crypto.activities.MathTesting.ltcstr;


/**
 * Created by andrew.kenreich on 8/14/2017.
 */

public class HomeChartFragment extends Fragment {

    //    assigning everthing
//set balance and bal of coins
    public static String email = "";
    public static String token = "";
    public static float balance = 0;
    public static float ethbalance = 0;
    public static float ltcbalance = 0;
    public static float btcbalance = 0;
    public static float ethprice = 0;
    public static float ltcprice = 0;
    public static float btcprice = 0;
    public static PieChart chart ;
    ArrayList<PieEntry> ENTRY ;
    ArrayList<String> EntryLabels ;
    ArrayList<LegendEntry> legends;
    PieDataSet dataset ;
    PieData DATA ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){




        //get shared pref email and then call GetBalance to get the balance of user.
        SharedPreferences loginPrefs = this.getContext().getSharedPreferences("loginPrefs", MODE_PRIVATE);
        email = loginPrefs.getString("email", "email not found");
        token = loginPrefs.getString("userToken", "token not found");




        //have to delcare these in this order to get the code to work right - rootView.findview has to be done in a Fragment
        View rootView = inflater.inflate(R.layout.fragment_donutchart, container, false);
        //get the chart id for this view.
        chart = (PieChart) rootView.findViewById(R.id.homepagechart);
        //get coin pricing
        GetAllCoins();
        //get balance
        GetBalance();
            return rootView;


    }
    public void DrawChart(){
        //assigning chart to view


        //setting up array list of values
        ENTRY = new ArrayList<>();
        EntryLabels = new ArrayList<>();
        legends = new ArrayList<>();

        //calling method to add data and labels
        AddValuesToENTRY();
        //AddValuesToEntryLabels();
        AddValuestoLabels();

        //setting the bottom left map guide
        dataset = new PieDataSet(ENTRY, "Cool Stuff");

        DATA = new PieData(dataset);


        //dataset styling - https://github.com/PhilJay/MPAndroidChart/wiki/The-DataSet-class
        //https://github.com/PhilJay/MPAndroidChart/wiki/General-Chart-Settings-&-Styling
        //makes lots of cool colors
        //dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        //For set the same color to chart
        dataset.setColors(new int[]{(ResourcesCompat.getColor(getResources(),R.color.piechartCash,null)), (ResourcesCompat.getColor(getResources(),R.color.piechartPurple,null)), (ResourcesCompat.getColor(getResources(),R.color.piechartYellow,null)), (ResourcesCompat.getColor(getResources(),R.color.piechartBlue,null))});

        //set the size of text for percentages in dp
        dataset.setValueTextSize(15);

        dataset.setValueFormatter(new PercentFormatter());




        //setting the data from above to the chart
        chart.setUsePercentValues(true);

        //stupid BS to set the description...why make it so difficult....
        Description description = new Description();
        description.setTextColor(ColorTemplate.VORDIPLOM_COLORS[2]);
        description.setTextSize(22);
        description.setText("Holdings %");

        //configue center hole radius
        chart.setHoleRadius(20);
        chart.setTransparentCircleRadius(20);
        chart.setDescription(description);
        chart.setData(DATA);
        chart.animateXY(2000, 2000);


        // customize legends
        Legend l = chart.getLegend();

        //placing the legend
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        //l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setFormToTextSpace(5);

        //set space between the legends
        l.setXEntrySpace(50);


        // set custom labels
        l.setCustom(legends);


        //text size of Legend text
        l.setTextSize(15);
        l.setTextColor(Color.BLACK);

        //telling X axis to be on bottom and not top...theres a few others you can do too
        //chart.getXAxis().setPosition(BOTTOM);

        //setting the labels from below to the X axis
        //chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(EntryLabels));

        //this animates it so the bars will grow up on the Y slowly. - set to 0 to just draw.
        //chart.animateY(3000);

    }




    public void AddValuesToENTRY(){


        //first value is the data, can do , <label> for a label to show under
        //these have to be floats so calling them as floats.
        ENTRY.add(new PieEntry(balance));
        ENTRY.add(new PieEntry(btcbalance));
        ENTRY.add(new PieEntry(ethbalance));
        ENTRY.add(new PieEntry(ltcbalance));
        Log.d("balance", String.valueOf(balance));

    }
    public void AddValuestoLabels(){

        //adding the legends - you can make LegendForm.CIRCLE, LegendForm.Line to have lines or circlies instead of the square - numbers set the size.
        //make sure these colors match the colors set in the dataset



        legends.add(new LegendEntry("CASH", Legend.LegendForm.DEFAULT,10f,2f,null, ResourcesCompat.getColor(getResources(),R.color.piechartCash,null)));
        legends.add(new LegendEntry("BTC", Legend.LegendForm.DEFAULT,10f,2f,null, ResourcesCompat.getColor(getResources(),R.color.piechartPurple,null)));
        legends.add(new LegendEntry("ETH", Legend.LegendForm.DEFAULT,10f,2f,null, ResourcesCompat.getColor(getResources(),R.color.piechartYellow,null)));
        legends.add(new LegendEntry("LTC", Legend.LegendForm.DEFAULT,10f,2f,null, ResourcesCompat.getColor(getResources(),R.color.piechartBlue,null)));



    }


    //public void AddValuesToEntryLabels(){

    //    EntryLabels.add("Cash");
    //    EntryLabels.add("BTC");
    //    EntryLabels.add("ETH");
    //    EntryLabels.add("LTC");
    //    EntryLabels.add("May");
    //    EntryLabels.add("June");

    //}

    //Get balance for user :)
    public void GetBalance() {


        OkHttpClient client = new OkHttpClient();
        //building balance call
        String btcbal = "http://104.131.0.17:8080/api/v1/users/"+email+"/balance";

        Request request = new Request.Builder()
                .url(btcbal)
                .header("x-access-token", token)
                .build();

        //Log.d("url", btcbal);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        String jsonData = null;
                        try {
                            jsonData = response.body().string();
                            Log.d("jsonData", jsonData);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        JSONArray Jarray= null;
                        JSONObject Jobject = null;
                        try {
                            Jobject = new JSONObject(jsonData);
                            //get coinbalance then get each object in coin balance - 0 - 4 and assign them to the object they are
                            JSONArray coinb = Jobject.getJSONArray("coinbalance");
                            JSONObject ethobj = coinb.getJSONObject(0);
                            JSONObject ltcobj = coinb.getJSONObject(1);
                            JSONObject btcobj = coinb.getJSONObject(2);
                            JSONObject cashobj = coinb.getJSONObject(3);

                            //get the string values of the obj's then convert them to doubles
                            ethstr = ethobj.getString("balance");
                            ltcstr = ltcobj.getString("balance");
                            btcstr = btcobj.getString("balance");
                            cashstr = cashobj.getString("balance");

                            ethbalance = Float.parseFloat(ethstr);
                            ltcbalance = Float.parseFloat(ltcstr);
                            btcbalance = Float.parseFloat(btcstr);
                            balance = Float.parseFloat(cashstr);


                            //converting the coins to dollar value.
                            ethbalance = ethbalance * ethprice;
                            ltcbalance = ltcbalance * ltcprice;
                            btcbalance = btcbalance * btcprice;



                            //draw chart after balance retrieved
                            DrawChart();
                            //set listener for chart
                            //SetListener();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
    }

    //Get coin info for each coin
    public void GetAllCoins() {


        OkHttpClient client = new OkHttpClient();
        //building balance call
        String prices = "http://104.131.0.17:8081/api/p1/prices";

        Request request = new Request.Builder()
                .url(prices)
                .build();

        //Log.d("url", btcbal);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        String jsonData = null;
                        try {
                            jsonData = response.body().string();
                            //Log.d("jsonData", jsonData);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        JSONArray Jarray= null;
                        JSONObject Jobject = null;
                        try {
                            Jarray = new JSONArray(jsonData);
                            //get each jsonobject from the array
                            //JSONArray coinb = Jobject.getJSONArray("coinbalance");
                            //Log.d("coinb", String.valueOf(Jarray));
                            JSONObject ethobj = Jarray.getJSONObject(1);
                            JSONObject ltcobj = Jarray.getJSONObject(2);
                            JSONObject btcobj = Jarray.getJSONObject(0);
                            //Log.d("coinb1", String.valueOf(ethobj));
                            //Log.d("coinb2", String.valueOf(ltcobj));
                            //Log.d("coinb3", String.valueOf(btcobj));
                            //get the string values of the obj's then convert them to doubles
                            String ethstrprice = ethobj.getString("price");
                            String ltcstrprice = ltcobj.getString("price");
                            String btcstrprice = btcobj.getString("price");

                            ethprice = Float.parseFloat(ethstrprice);
                            ltcprice = Float.parseFloat(ltcstrprice);
                            btcprice = Float.parseFloat(btcstrprice);




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
    }

}
