package com.example.adalligo.crypto.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.helper.MenuToolbarHelper;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

/**
 * Created by andrew.kenreich on 6/30/2017.
 * https://www.tradingview.com/widget/advanced-chart/ - code for widget is here
 */

public class TradingViewChart extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.chart);


            // Toolbar & Menu

            // Sets toolbar given context and toolbar view ID
            MenuToolbarHelper.setToolbar(this, R.id.toolbar, "Charting");

            // Menu
            NavigationView navMenu = (NavigationView) findViewById(R.id.navView);
            MenuToolbarHelper.createMenu(this, navMenu);


            WebView myWebView = (WebView) findViewById(R.id.webviewchart);

            //enable java script websettings
            WebSettings webSettings = myWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            //myWebView.loadUrl("https://s3.tradingview.com/tv.js");
            //put code for custom html here - so the code for the widget goes here.
            String customHtml = "<!-- TradingView Widget BEGIN -->\n" +
                    "<script type=\"text/javascript\" src=\"https://s3.tradingview.com/tv.js\"></script>\n" +
                    "<script type=\"text/javascript\">\n" +
                    "new TradingView.widget({\n" +
                    "  \"autosize\": true,\n" +
                    "  \"symbol\": \"BTCUSD\",\n" +
                    "  \"interval\": \"D\",\n" +
                    "  \"timezone\": \"Etc/UTC\",\n" +
                    "  \"theme\": \"Black\",\n" +
                    "  \"style\": \"1\",\n" +
                    "  \"locale\": \"en\",\n" +
                    "  \"toolbar_bg\": \"#f1f3f6\",\n" +
                    "  \"enable_publishing\": false,\n" +
                    "  \"allow_symbol_change\": true,\n" +
                    "  \"hideideas\": true\n" +
                    "});\n" +
                    "</script>\n" +
                    "<!-- TradingView Widget END -->";

            //load the custom html from above into the webview.
            myWebView.loadData(customHtml, "text/html", "UTF-8");

        }

}
