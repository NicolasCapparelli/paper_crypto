package com.example.adalligo.crypto.api;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.activities.Home;
import com.example.adalligo.crypto.activities.Login;
import com.example.adalligo.crypto.custom.OpenPosition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Nico on 6/24/2017.
 */

public class AdalligoAPI {

////// Constants //////

    // API URL
    public static String API_URL = "http://104.131.0.17:8080";
    public static String PRICE_API_URL = "http://104.131.0.17:8081";

    // OkHttpClient needed to make requests
    public static final OkHttpClient CLIENT = new OkHttpClient();

    // Media Type for post requests
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");


////// Specific Asynchronous Functions //////
/**
* The methods below are asynchronous tasks created for a particular use.
* For example: Logging the user in and storing the authentication token in user preferences
**/

    // Registers the user and logs them into the app
    public static void asyncRegister(final Context context, String username, final String email, final String pwd) throws JSONException {

        // Making a progress dialog show up
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("Registering...");
        dialog.show();

        // Building the request itself with the given attributes
        Request request = registerUser(username, email, pwd);

        // Running the request as an async task
        CLIENT.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                final String err = e.toString();

                // This handler is here because things like showing toasts and dismissing dialogs MUST be done on the MAIN thread
                // So what it does is it makes a new runnable back in the main thread to get it out of the async task, thus being able to show the toast and dismiss the dialog
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        // Dismissing dialog if it's still showing
                        if (dialog.isShowing()){
                            dialog.dismiss();
                        }

                        // Toast detailing network error
                        Toast.makeText(context, "A network error occurred, please try again later", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // Handler to take us back to main thread
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        // Removes dialog
                        if (dialog.isShowing()){
                            dialog.dismiss();
                        }

                        // if successful
                        if (response.code() == 201){
                            // Toast
                            Toast.makeText(context, "Registration Successful, Navigating To Login Screen", Toast.LENGTH_SHORT).show();

                            // Taking the user to login screen
                            Intent toLogin = new Intent(context, Login.class);
                            context.startActivity(toLogin);

                        }

                        else if (response.code() == 404){
                            // Toast
                            Toast.makeText(context, "There was an issue with registration", Toast.LENGTH_SHORT).show();
                        }

                        else if (response.code() == 409){
                            // Toast
                            Toast.makeText(context, "A user with this email has already been registered", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    // Logs the user in and store their authToken in a preference
    public static void asyncLogin(final Context context, final String email, final String pwd) throws Exception {

        // Making a progress dialog show up
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("Logging in...");
        dialog.show();

        // Building the request itself with the given attributes
        final Request request = userLogin(email, pwd);

        // Executing the request in an async task
        CLIENT.newCall(request).enqueue(new Callback() {

            // If it fails
            @Override
            public void onFailure(Call call, IOException e) {

                final String err = e.toString();

                // This handler is here because things like showing toasts and dismissing dialogs MUST be done on the MAIN thread
                // So what it does is it makes a new runnable back in the main thread to get it out of the async task, thus being able to show the toast and dismiss the dialog
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        // Dismissing dialog if it's still showing
                        if (dialog.isShowing()){
                            dialog.dismiss();
                        }
                        // Toast detailing network error
                        Toast.makeText(context, "A network error occurred, please try again later", Toast.LENGTH_LONG).show();
                    }
                });
            }

            // If it succeeds
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final Response resp = response;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        // Mutable string where the token will be placed
                        String token = "";

                        // Response code
                        int respCode = resp.code();

                        if (respCode == 200){

                            // Turning response to a string then creating a JSON object with the response string
                            String respString = "";
                            try {
                                respString = resp.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            // Unable to put exception in method signature, so im using a try/catch
                            // Getting the token from the response
                            JSONObject respJson;
                            try {
                                respJson = new JSONObject(respString);
                                token = respJson.getString("token");
                            } catch (JSONException e) {
                                respJson = null;
                                token = "error";
                                e.printStackTrace();
                            }

                            // Initiating Login Preferences
                            SharedPreferences loginPrefs = context.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
                            // Login Preference Editor
                            SharedPreferences.Editor loginEditor = loginPrefs.edit();
                            // Putting email in preference
                            loginEditor.putString("email", email);
                            // Putting pwd in preference TODO make sure this is encrypted
                            loginEditor.putString("pwd", pwd);
                            // Putting token in preference
                            loginEditor.putString("userToken", token);
                            // Setting isLoggedIn preference to true
                            loginEditor.putBoolean("isLoggedIn", true);

                            // Committing the changes
                            loginEditor.commit();

                            // Dismissing dialog if it's still showing
                            if (dialog.isShowing()){
                                dialog.dismiss();
                            }

                            // Takes the user to home page
                            Intent toHome = new Intent(context, Home.class);
                            context.startActivity(toHome);
                        }

                        // If email is not found in the db
                        else if (respCode == 401){

                            // Dismissing dialog if it's still showing
                            if (dialog.isShowing()){
                                dialog.dismiss();
                            }

                            // Toast telling the user that the credentials were invalid
                            Toast.makeText(context, "Invalid Credentials", Toast.LENGTH_LONG).show();
                        }

                        else if (respCode == 503){

                            // Dismissing dialog if it's still showing
                            if (dialog.isShowing()){
                                dialog.dismiss();

                                // Toast telling the user that the credentials were invalid
                                Toast.makeText(context, "There is an issue with our servers at the moment, please try again later", Toast.LENGTH_LONG).show();

                                Log.d("Response Code>>>>>>>", String.valueOf(respCode));

                            }
                        }

                        // Else
                        else {

                            // Dismissing dialog if it's still showing
                            if (dialog.isShowing()){
                                dialog.dismiss();
                            }

                            Log.d(">>>>>>>>", String.valueOf(respCode));
                        }
                    }
                });
            }
        });
    }


    // First part of password reset
    public static void asyncResetPwdPartOne(final Context context, final int viewID, String email) throws JSONException {

        // Loading Screen
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("Logging in...");
        dialog.show();

        // Building the request itself with the given attributes
        final Request request = resetPwdPartOne(email);

        // Launching the request and making a callback object
        CLIENT.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // TextView to change
                final TextView tvDirections = (TextView) ((Activity)context).findViewById(viewID);

                // Back to main thread
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        // Dismissing dialog if it's still showing
                        if (dialog.isShowing()){
                            dialog.dismiss();
                        }

                        // If success
                        if (response.code() == 200){
                            // Fragment for part two
                            Login.ResetPasswordFragmentTwo resPassFrag = new Login.ResetPasswordFragmentTwo();
                            // Change fragments and add the main fragment to back stack (handles what happens when the user presses the back button)
                            ((Activity) context).getFragmentManager().beginTransaction().replace(R.id.fragmentFrame,resPassFrag,resPassFrag.TAG).addToBackStack(resPassFrag.TAG).commit();

                        }

                        // If the email provided
                        else if (response.code() == 404) {
                            // Set the directions text to:
                            tvDirections.setText("The email provided does match an email in our database");
                        }

                        else {
                            // Set the directions text to:
                            tvDirections.setText("A network error occurred, please try again later");
                        }
                    }
                });

            }
        });
    }

    // Part two of reset password sequence
    // @Param: ChangeToken (Verification Code Sent To User), email, newPwd (New Password)
    public static void asyncResetPwdTwo(final Context context, final int viewID, String changeToken, String email, String newPwd) throws JSONException {

        // Loading Screen
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("Logging in...");
        dialog.show();

        // Building the request itself with the given attributes
        Request request = resetPwdTwo(viewID, changeToken, email, newPwd);

        // Making the call and creating callback methods
        CLIENT.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // The text view we will change in the activity if there is an issue
                final TextView tvInstructions = (TextView) ((Activity)context).findViewById(viewID);

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        // Dismissing dialog if it's still showing
                        if (dialog.isShowing()){
                            dialog.dismiss();
                        }

                        // Success tree
                        if (response.code() == 200){
                            // Fragment
                            Login.MainFragment mainFragment = new Login.MainFragment();
                            // Change fragments and add the main fragment to back stack (handles what happens when the user presses the back button)
                            ((Activity) context).getFragmentManager().beginTransaction().replace(R.id.fragmentFrame,mainFragment,mainFragment.TAG).addToBackStack(mainFragment.TAG).commit();

                            // Toast telling the user of success
                            Toast.makeText(context, "Your password was successfully changed", Toast.LENGTH_LONG).show();
                        }

                        else {
                            // TODO: error handling here, need to ask andrew why there's no password reset function on server
                            tvInstructions.setText("There was an error with the authentication code, but Andrew was not here lol");

                        }
                    }
                });
            }
        });
    }




////// Individual Methods (Request Builders) //////

/**
* The methods below return OkHTTP Request Objects. These methods can be called to create a
* Request in an AsyncTask above that serves a specific purpose, like populating a View
**/

    // Registers User
    private static Request registerUser(String username, String email, String pwd) throws JSONException {

        // URI of the API
        String URL = API_URL + "/api/v1/users/";

        // Creates JSON payload, stills needs to be built with values
        JSONObject payload = new JSONObject();

        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        payload.put("name", username);
        payload.put("email", email);
        payload.put("password", pwd);

        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(payload));

        // Building the request itself with the given attributes
        Request request = new Request.Builder()
                .url(URL)
                .post(params)
                .build();

        return request;
    }

    // Logs the user in and store their authToken in a preference
    public static Request userLogin(String email, String pwd) throws Exception {

        // URI of the API
        String URL = API_URL + "/api/v1/authenticate";

        // Putting the credentials into one string
        String credentials = email + ":" + pwd;

        // Encoding/Encrypting that string with Base64
        String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        // Empty body
        RequestBody params = RequestBody.create(JSON, "");

        // Building the request itself with the given attributes
        final Request request = new Request.Builder()
                .url(URL)
                .header("Authorization", basic)
                .post(params)
                .build();

        return request;
    }


    // First part of password reset
    public static Request resetPwdPartOne(String email) throws JSONException {

        // URL
        String URL = API_URL + "/api/v1/users/" + email + "/password";

        // Creates JSON payload, stills needs to be built with values
        JSONObject payload = new JSONObject();

        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        payload.put("", "");

        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(payload));

        // Building the request itself with the given attributes
        return new Request.Builder()
                .url(URL)
                .post(params)
                .build();
    }

    // Part two of reset password sequence
    // @Param: ChangeToken (Verification Code Sent To User), email, newPwd (New Password)
    public static Request resetPwdTwo(int viewID, String changeToken, String email, String newPwd) throws JSONException {

        // URL
        String URL = API_URL + "/api/v1/users/" + email + "/password";

        // Creates JSON payload, stills needs to be built with values
        JSONObject payload = new JSONObject();

        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        payload.put("token", changeToken);
        payload.put("password", newPwd);


        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(payload));

        // Building the request itself with the given attributes
        return new Request.Builder()
                .url(URL)
                .post(params)
                .build();
    }


    // Open positions
    public static Request getUserOpenPositions(Context context) throws JSONException, IOException {

        // Login Preferences to get user token
        SharedPreferences loginPrefs = context.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);

        // Getting the token from shared preferences
        String token = loginPrefs.getString("userToken", "");


        // Getting email from loginPrefs
        String email = loginPrefs.getString("email", "");

        // URL
        String URL = API_URL + "/api/v1/users/" + email + "/openpositions";

        // Building the request itself with the given attributes
        return new Request.Builder()
                .url(URL)
                .header("x-access-token", token)
                .build();
    }

//// Price API ////


    // Coin prices for all coins
    public static Request getAllCoinPrices() throws JSONException, IOException {

        // URL
        String URL = PRICE_API_URL + "/api/p1/prices/";

        // Building the request itself with the given attributes
        return  new Request.Builder()
                .url(URL)
                .build();
    }


    // Coin Prices For Individual Coins
    public static Request getCoinPrice(String coinAbbreviation) throws JSONException, IOException {

        // URL
        String URL = PRICE_API_URL + "/api/p1/price/" + coinAbbreviation;

        // Building the request itself with the given attributes
        return  new Request.Builder()
                .url(URL)
                .build();
    }


    // 24 Hour Price Data

    // Coin Prices
    public static Request get24PriceData() throws JSONException, IOException {

        // URL
        String URL = PRICE_API_URL + "/api/p1/24hprice/";

        // Building the request itself with the given attributes and returning it
        return new Request.Builder()
                .url(URL)
                .build();

    }
}
