package com.example.adalligo.crypto.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.activities.Home;
import com.example.adalligo.crypto.activities.Login;
import com.example.adalligo.crypto.activities.MathTesting;
import com.example.adalligo.crypto.activities.Positions;
import com.example.adalligo.crypto.activities.RssFeed;
import com.example.adalligo.crypto.activities.TradingViewChart;
import com.example.adalligo.crypto.activities.Welcome;

/**
 * Created by Nico on 7/2/2017.
 */

public class MenuToolbarHelper {

    // Instantiates the toolbar
    public static void setToolbar(Context context, int toolbarViewID, String toolbarTitle){

        // Toolbar
        Toolbar toolbar = (Toolbar) ((Activity)context).findViewById(toolbarViewID);

        // Sets the action bar for the activity
        ((AppCompatActivity)context).setSupportActionBar(toolbar);

        // Gets the toolbar we just set and gives it a title
        ((AppCompatActivity)context).getSupportActionBar().setTitle(toolbarTitle);

        // The drawer layout
        DrawerLayout drawerLayout = (DrawerLayout) ((Activity)context).findViewById(R.id.drawer_layout);

        // The hamburger menu button: The string parameters are needed for accessibility purposes
        ActionBarDrawerToggle drawerButton = new ActionBarDrawerToggle(((Activity)context), drawerLayout, toolbar, R.string.drawerOpen, R.string.drawerClose);

        // Tells the layout to add a drawer listener to the ActionBarDrawerToggle above
        drawerLayout.addDrawerListener(drawerButton);

        // Syncs the state of the drawer (I believe this means whether the drawer is in or out) with the view?
        drawerButton.syncState();
    }

    // Creates a menu for  the given class
    public static void createMenu(final Context currentActivity, NavigationView menu){

        menu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();
                if (id == R.id.itemOne) {
                    Intent openLogin = new Intent(currentActivity, Home.class);
                    currentActivity.startActivity(openLogin);
                    return false;
                }
                if (id == R.id.itemTwo) {
                    Intent openWatch = new Intent(currentActivity, MathTesting.class);
                    currentActivity.startActivity(openWatch);
                    return false;
                }
                if (id == R.id.itemThree) {
                    Intent openScanner = new Intent(currentActivity, Positions.class);
                    currentActivity.startActivity(openScanner);
                    return false;
                }
                if (id == R.id.itemFour) {
                    Intent openScanner = new Intent(currentActivity, Login.class);
                    currentActivity.startActivity(openScanner);
                    return false;
                }

                if (id == R.id.itemFive) {
                    Intent openScanner = new Intent(currentActivity, Login.class);
                    currentActivity.startActivity(openScanner);
                    return false;
                }
                if (id == R.id.itemSix) {
                    Intent openScanner = new Intent(currentActivity, RssFeed.class);
                    currentActivity.startActivity(openScanner);
                    return false;
                }
                return false;
            }
        });
    }



}
