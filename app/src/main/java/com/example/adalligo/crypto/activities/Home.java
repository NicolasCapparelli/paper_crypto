package com.example.adalligo.crypto.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.adapters.ChartAdapter;
import com.example.adalligo.crypto.adapters.CoinPriceScrollAdapter;
import com.example.adalligo.crypto.custom.CoinPriceScroll;
import com.example.adalligo.crypto.data.UpdatedCoinPriceData;
import com.example.adalligo.crypto.helper.MenuToolbarHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.example.adalligo.crypto.R.id.pager;

public class Home extends AppCompatActivity {
    Context thisActivity;
    ScheduledThreadPoolExecutor coinPriceScheduler;
    ScheduledThreadPoolExecutor selfScrollScheduler;
    CoinPriceScrollAdapter adapter;
    RecyclerView rvPriceHolder;
    TextView tvEmail;
    TextView tvPwd;
    TextView tvIsLoginRemembered;

    //declaring adapter
    ChartAdapter adapterchart;
    ViewPager pager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //load fragment and
        //set adapter to pager
        adapterchart = new ChartAdapter(getSupportFragmentManager());
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapterchart);

        //swipe stuff for pageadapter - adding the listener methods
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Context
        thisActivity = this;

        // Date

        Calendar cal = Calendar.getInstance();
        Log.d(">>>>>>>>>", cal.getTime().toString());

        // SharedPreferences
        SharedPreferences loginPrefs = getSharedPreferences("loginPrefs", MODE_PRIVATE);

        // Toolbar & Menu

        // Sets toolbar given context and toolbar view ID
        MenuToolbarHelper.setToolbar(this, R.id.toolbar, "CryptoDB");

        // Menu
        NavigationView navMenu = (NavigationView) findViewById(R.id.navView);
        MenuToolbarHelper.createMenu(this, navMenu);

        // Testing views
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvPwd = (TextView) findViewById(R.id.tvPwd);
        tvIsLoginRemembered = (TextView) findViewById(R.id.tvIsLogin);

        // Setting stuff
        tvEmail.setText(loginPrefs.getString("email", "email not found"));
        tvPwd.setText(loginPrefs.getString("pwd", "pass not found"));
        tvIsLoginRemembered.setText(String.valueOf(loginPrefs.getBoolean("isRememberedLogin", false)));

        // Marquee

        // Price Data
        final UpdatedCoinPriceData priceData = new UpdatedCoinPriceData();

        // Adapter
        adapter = new CoinPriceScrollAdapter(this);

        // Recycler View
        rvPriceHolder = (RecyclerView) findViewById(R.id.rvPriceHolder);

        // Setting layout manager (Horizontal)
        rvPriceHolder.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));

        // Attaching adapter
        rvPriceHolder.setAdapter(adapter);

        // Timer Task that will run the GET for the prices every X seconds
        coinPriceScheduler = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);

        // Timer Task that autoScrolls the top recycler view
        selfScrollScheduler = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);

        // Inserting initial data into adapter

        ArrayList<CoinPriceScroll> initialData = new ArrayList<>();

        initialData.add(new CoinPriceScroll("BTC"));
        initialData.add(new CoinPriceScroll("ETH"));
        initialData.add(new CoinPriceScroll("LTC"));

        adapter.setViewableDataList(initialData);

        // Scrolls to the first position in recycler view so it is always reset before running the auto switcher method
        rvPriceHolder.smoothScrollToPosition(0);

        // Method that runs in another thread, responsible for auto switching the prices at the top of the screen
        autoSwitcher();

        // Scheduling the task
        coinPriceScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    // Run the GET for prices
                    priceData.get24HourPrices();
                }

                catch (Exception e) {
                    e.printStackTrace();
                    Log.d(">>>Executor Err/", e.toString());
                }
            }
        }, 0, 15000, TimeUnit.MILLISECONDS);

        // Listener for price data changes
        priceData.setPriceChangeListener(new UpdatedCoinPriceData.onPriceUpdateListener() {
            @Override
            public void onPriceChange() {

                // Array of coinTypes
                String[] coins = {"BTC", "ETH", "LTC"};

                // For coin type in array
                for (String coinType: coins){
                    // Get position of CoinPriceScroll object with given coinType
                    int coinPos = adapter.getCoinPosition(coinType);

                    // If the coinType isn't present in the data list
                    if (coinPos == -1){
                        Log.d(">>>>>>>>>", coinType + " Is not in data list");
                    }

                    else {

                        Bundle b = new Bundle();

                        // Set the
                        adapter.setSpecificData(priceData.getCoinData(coinType), coinPos);
                        ((Activity)thisActivity).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();

        coinPriceScheduler.shutdown();

    }

    @Override
    protected void onStop() {
        super.onStop();
        coinPriceScheduler.shutdown();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        coinPriceScheduler.shutdown();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    // Methods

    // Responsible for auto switching the prices at the top of the screen, runs in a seperate thread
    public void autoSwitcher(){

        // Delay between each switch in milliseconds
        final long switchDelay = 10000;

        // Handler to delay the runnable
        final Handler handler = new Handler();

        // Runnable to handle switching (Does switching in another thread)
        final Runnable runnable = new Runnable() {

            // counter to keep track of what position to switch to
            int count = 0;

            // boolean to let the program know to start switching in reverse order
            boolean isReversing = false;

            @Override
            public void run() {

                // If counter has reached the last CoinScroll
                if(count == adapter.getItemCount() - 1){
                    isReversing = true;
                }

                // If the counter reaches the beginning
                if (count == 0){
                    isReversing = false;
                }

                // If the counter is less than the total number of positions in the recycler view AND it is not reversing
                if(count <= adapter.getItemCount() -1  && !isReversing){

                    // Increment counter
                    count++;
                }

                // If the counter is less than the total number of positions in the recycler view BUT it IS reversing
                else {
                    // Decrement counter
                    count--;
                }

                // Tells the recycler view to switch to the view in the position that the counter is currently at
                rvPriceHolder.smoothScrollToPosition(count);

                // Waits X milliseconds (switchDelay), then, runs this method again
                handler.postDelayed(this, switchDelay);

            }
        };
        handler.postDelayed(runnable, switchDelay);


    }

}


