package com.example.adalligo.crypto.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.adapters.OpenPositionAdapter;
import com.example.adalligo.crypto.api.AdalligoAPI;
import com.example.adalligo.crypto.custom.OpenPosition;
import com.example.adalligo.crypto.data.CoinPriceData;
import com.example.adalligo.crypto.data.OpenPositionData;
import com.example.adalligo.crypto.helper.MenuToolbarHelper;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;


public class Positions extends AppCompatActivity {

    // Fields
    private Context thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_positions);

        // Context
        thisActivity = this;

        // Toolbar & Menu

        // Sets toolbar given context and toolbar view ID
        MenuToolbarHelper.setToolbar(this, R.id.toolbar, "Positions");

        // Menu
        NavigationView navMenu = (NavigationView) findViewById(R.id.navView);
        MenuToolbarHelper.createMenu(this, navMenu);

        // Tab Layout

        // View Pager Instantiation
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);

        // Pager Adapter (Links pager with code)
        TabPagerAdapter pagerAdapter= new TabPagerAdapter(getSupportFragmentManager());

        // Setting the two together
        viewPager.setAdapter(pagerAdapter);

        // Tab Layout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        // Link with view pager
        tabLayout.setupWithViewPager(viewPager);

    }

    // An adapter for the TabView, which is where it gets all of it's information from
    public static class TabPagerAdapter extends FragmentPagerAdapter {
        // Constructor
        public TabPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        // Amount of "Views" (Tabs) The TabView will have, in our case 2 views (Live and Closed Positions)
        @Override
        public int getCount() {
            return 2;
        }

        // Returns the Page Title for the given position
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){

                // Position one (Open Positions Tab)
                case 0:
                    return "Open";

                // Position two (Closed Positions Tab)
                case 1:
                    return "Closed";

                default:
                    return null;
            }
        }

        // Return the fragment in X position (Tab 0 or Tab 1 in this case)
        @Override
        public android.support.v4.app.Fragment getItem(int position) {

            switch (position){
                // Position 0
                case 0:
                    return new LiveOrdersFragment();

                // Position 1
                case 1:
                    return new ClosedOrdersFragment();

                default:
                    return null;
            }
        }
    }

    // Fragment to hold live orders
    public static class LiveOrdersFragment extends android.support.v4.app.Fragment {

        // Fields
        Context activity;
        OpenPositionAdapter adapter;
        ContentLoadingProgressBar lbLoading;
        RelativeLayout ltContainer;
        RelativeLayout ltExtendedView;
        LinearLayout ltDetails;
        TextView tvDetailView;
        ImageButton ibDotOptions;
        ImageButton ibFilter;
        RecyclerView rvPositions;
        FloatingActionButton fbAddPosition;

        CoinPriceData prices;
        ScheduledThreadPoolExecutor coinPriceScheduler;


        private static final String TAG = "LiveOrdersFragment";

        public LiveOrdersFragment(){
            super();
            // Empty Constructor
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Instantiating the adapter
            adapter = new OpenPositionAdapter(getContext());
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            // The main view
            View mainView = inflater.inflate(R.layout.fragment_open_positions, container, false);
            mainView.setTag(TAG);

            // Context
            activity = getContext();

            // Init Views

            lbLoading = (ContentLoadingProgressBar) mainView.findViewById(R.id.lbLoading);
            ltDetails = (LinearLayout) mainView.findViewById(R.id.ltDetails);
            tvDetailView = (TextView) mainView.findViewById(R.id.tvDetailView);
            ibDotOptions = (ImageButton) mainView.findViewById(R.id.ibDotOptinos);
            ibFilter = (ImageButton) mainView.findViewById(R.id.ibFilter);
            fbAddPosition = (FloatingActionButton) mainView.findViewById(R.id.fbAddPosition);

            // Shows loading bar while trying to get position information
            lbLoading.bringToFront();
            lbLoading.show();


            // Recycler View
            rvPositions = (RecyclerView) mainView.findViewById(R.id.rvPositions);

            // Setting layout manger
            rvPositions.setLayoutManager(new LinearLayoutManager(getContext()));

            // Linking the adapter to the recycler view
            rvPositions.setAdapter(adapter);

            // Filter button listener
            ibFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Instantiates the popup menu (Overflow menu), passing in the activity and the view that was clicked
                    PopupMenu popup = new PopupMenu(getContext(), v);

                    // Getting the inflater for the popup
                    MenuInflater inflater = popup.getMenuInflater();

                    // Inflating the menu with the menu resource
                    inflater.inflate(R.menu.coin_filter_menu, popup.getMenu());

                    // Set the menu item click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.filterMenuZero:
                                    adapter.setCompleteDataListAsViewable();
                                    adapter.notifyDataSetChanged();
                                    return true;
                                case R.id.filterMenuOne:
                                    adapter.filterViewableListByCoin("BTC");
                                    adapter.notifyDataSetChanged();
                                    return true;
                                case R.id.filterMenuTwo:
                                    adapter.filterViewableListByCoin("ETH");
                                    adapter.notifyDataSetChanged();
                                    return true;

                                case R.id.filterMenuThree:
                                    adapter.filterViewableListByCoin("LTC");
                                    adapter.notifyDataSetChanged();
                                    return true;

                                default:
                                    return false;
                            }
                        }
                    });
                    // Show the menu
                    popup.show();
                }
            });


            // Options button listener
            ibDotOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            fbAddPosition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toOrderPlace = new Intent(getContext(), MathTesting.class);
                    getContext().startActivity(toOrderPlace);
                }
            });

            // Attaching ItemTouchHelper
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCallback());

            // Attach the touchHelper to our recycler view
            itemTouchHelper.attachToRecyclerView(rvPositions);

            // Attempt to populate recycler view
            try {
                asyncGetUserOpenPositions();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // What happens when recycler view item is clicked
            adapter.setItemClickCallback(new OpenPositionAdapter.ItemClickCallback() {
                @Override
                public void onItemClick(final int p) {
                    Log.d(">>>>>>>>>>>>>>>>", "IN HERE");

                    // If the extended view IS extended
                    if (adapter.getSpecificData(p).getIsExtendedShowing()){

                        // Set visibility false
                        adapter.setExtendedViewVisibility(p, false);
                        Log.d(">>>>>>>>>>>>>>>>", "TRUUUUU");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyItemChanged(p);
                            }
                        });

                    }

                    // If the extended view IS extended
                    else {
                        // set visibility true
                        adapter.setExtendedViewVisibility(p, true);
                        Log.d(">>>>>>>>>>>>>>>>", "FALSSEEE");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyItemChanged(p);
                            }
                        });
                    }
                }
            });

            // Coin prices
            prices = new CoinPriceData();

            // Timer Task that will run the GET for the prices every X seconds
            coinPriceScheduler = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);

            // Scheduling the task
            coinPriceScheduler.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Run the GET for prices
                        prices.asyncGetBTCPrice();
                        prices.asyncGetETHPrice();
                        prices.asyncGetLTCPrice();
                    }

                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 0, 5000, TimeUnit.MILLISECONDS);


            // BTC Price Change Listener
            prices.setOnBitcoinPriceChangeListener(new CoinPriceData.onBitcoinPriceChangeListener() {
                @Override
                public void onBtcPriceChange() {

                    // Get new price
                    final double newBtcPrice = prices.getBtcPrice();
                    // Bundle to be passed
                    final Bundle updatePriceBundle = new Bundle();

                    // Adding coinType to bundle
                    updatePriceBundle.putString("coinType", "BTC");

                    // Adding newPrice to bundle
                    updatePriceBundle.putString("newPrice", String.valueOf(newBtcPrice));

                    // Array with the positions where OpenPosition views have the coinType BTC are located
                    final ArrayList<Integer> btcPositionValues = adapter.getPositionsWithCoinType("BTC");

                    // Back to main thread
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Iterate through btcPositionValue Array
                            for (int i = 0; i < btcPositionValues.size(); i++){

                                // Sets  the value for ALL the OpenPositions that are BTC so that when the user scrolls down the views are synced with price
                                adapter.getSpecificData(btcPositionValues.get(i)).setCurrentPrice(newBtcPrice);

                                // Changes any views that are currently VIEWABLE in the recycler view and updates the price
                                adapter.notifyItemChanged(btcPositionValues.get(i), updatePriceBundle);
                            }
                        }
                    });
                }
            });

            // ETH Price Change Listener
            prices.setOnEtherumPriceChangeListener(new CoinPriceData.onEthereumPriceChangeListener() {
                @Override
                public void onEthPriceChange() {

                    // Get new price
                    final double newEthPrice = prices.getEthPrice();

                    final Bundle updatePriceBundle = new Bundle();

                    // Adding cointype to bundle
                    updatePriceBundle.putString("coinType", "ETH");

                    // Adding newPrice to bundle
                    updatePriceBundle.putString("newPrice", String.valueOf(newEthPrice));

                    // Array with the positions where OpenPosition views have the cointype BTC are located
                    final ArrayList<Integer> ethPositionValues = adapter.getPositionsWithCoinType("ETH");

                    // Back to main thread
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Iterate through btcPositionValue Array
                            for (int i = 0; i < ethPositionValues.size(); i++){

                                // Sets  the value for ALL the OpenPositions that are ETH so that when the user scrolls down the views are synced with price
                                adapter.getSpecificData(ethPositionValues.get(i)).setCurrentPrice(newEthPrice);

                                // Changes any views that are currently VIEWABLE in the recycler view and updates the price
                                adapter.notifyItemChanged(ethPositionValues.get(i), updatePriceBundle);
                            }
                        }
                    });
                }
            });

            // LTC price change listener
            prices.setOnLitecoinPriceChangeListener(new CoinPriceData.onLitecoinPriceChangeListener() {
                @Override
                public void onLtcPriceChange() {
                    final double newLtcPrice = prices.getLtcPrice();
                    // Creating bundle
                    final Bundle updatePriceBundle = new Bundle();
                    // Adding cointype to bundle
                    updatePriceBundle.putString("coinType", "LTC");
                    // Adding newPrice to bundle
                    updatePriceBundle.putString("newPrice", String.valueOf(newLtcPrice));
                    // adapter.updateCoinPrices("LTC", newLtcPrice);

                    // Array with the positions where OpenPosition views have the cointype BTC are located
                    final ArrayList<Integer> ltcPositionValues = adapter.getPositionsWithCoinType("LTC");

                    // Back to main thread
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Iterate through btcPositionValue Array
                            for (int i = 0; i < ltcPositionValues.size(); i++){

                                // Sets  the value for ALL the OpenPositions that are LTC so that when the user scrolls down the views are synced with price
                                adapter.getSpecificData(ltcPositionValues.get(i)).setCurrentPrice(newLtcPrice);

                                // Changes any views that are currently VIEWABLE in the recycler view and updates the price
                                adapter.notifyItemChanged(ltcPositionValues.get(i), updatePriceBundle);
                            }
                        }
                    });

                }
            });

            return mainView;
        }

            @Override
        public void onPause() {
            super.onPause();
            // On Fragment pause, shutdown GET prices
            coinPriceScheduler.shutdown();
        }

        @Override
        public void onStop() {
            super.onStop();
            // On Fragment Stop, shutdown GET prices timer
            coinPriceScheduler.shutdown();
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            coinPriceScheduler.shutdown();
        }

        @Override
        public void onResume() {
            super.onResume();

            // On fragment resume, start the GET Prices again
            coinPriceScheduler = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);
            coinPriceScheduler.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Run the GET for prices
                        prices.asyncGetBTCPrice();
                        prices.asyncGetETHPrice();
                        prices.asyncGetLTCPrice();
                    }

                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 0, 5000, TimeUnit.MILLISECONDS);

        }

        // Get Open Positions and Populate Recycler View
        private void asyncGetUserOpenPositions() throws JSONException, IOException {

            final ArrayList<OpenPosition> positionData = new ArrayList<>();

            final Request request = AdalligoAPI.getUserOpenPositions(getContext());
            AdalligoAPI.CLIENT.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    ArrayList<OpenPosition> firstData = new ArrayList<>();

                    // Attempt to parse response
                    try {
                        firstData = OpenPositionData.parseResponse(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // Setting the data
                    adapter.setCompleteDataList(firstData);

                    // Makes the complete data list viewable
                    adapter.setCompleteDataListAsViewable();

                    // Back to main loop
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {

                            // Hide loading screen
                            lbLoading.hide();

                            // Tell the adapter the data list has been updated
                            adapter.notifyDataSetChanged();

                        }
                    });
                }
            });
        }

        // Boiler plate code for the ItemTouchCallback
        private ItemTouchHelper.Callback createHelperCallback() {
            // Used for drawing below our OpenPosition
            final Paint p = new Paint();
            ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                    // This si the line that allows the user to move items UP and DOWN or swipe LEFT and RIGHT
                    // If you want to remove any of those functionalities, you can replace the ItemTouchHelper.(Functionality) with a 0
                    // For example, changing ItemTouchHelper.UP | ItemTouchHelper.DOWN to 0 will remove the ability to move teh view UP and DOWN
                    new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                        // This is the method that handles re-arranging the views, if this is not a functionality you want, the inner part of the method can be commented out
                        // The viewHolder is the item you've long clicked to move, the target is the item you're trying to move the item to
                        @Override
                        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                              RecyclerView.ViewHolder target) {
                            moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                            return true;
                        }

                        // This handles what happens when the view is swiped
                        // For swipe directions, add if statement for int swipeDir
                        @Override
                        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                            int position = viewHolder.getAdapterPosition();

                            // If swipe direction == left, do this
                            if(swipeDir == ItemTouchHelper.LEFT){

                                // Tells the adapter that the item was changed so it can revert it to its normal position
                                adapter.notifyItemChanged(position);
                            }

                            // If swiped left
                            else if(swipeDir == ItemTouchHelper.RIGHT) {
                                // OpenPosition we swiped
                                OpenPosition positionSwiped = (OpenPosition)   adapter.getSpecificData(position);

                                // Tells the adapter that the item was changed so it can revert it to its normal position
                                adapter.notifyItemChanged(position);
                            }
                        }

                        // This method draws the views underneath the OpenPositions once it is swiped
                        // @Param dX: amount of horizontal displacement caused by the user
                        @Override
                        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                            Bitmap icon;
                            if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                                View itemView = viewHolder.itemView;
                                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                                float width = height / 3;

                                // If the displacement of the view is greater than 0 (Going right)
                                if(dX > 0){
                                    // Sets the background color for the underlying view ()
                                    p.setColor(Color.parseColor("#6C27C8"));

                                    // Creates a rectangle given the dimensions of the OpenPosition view above it
                                    RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());

                                    // Draws said rectangle
                                    c.drawRect(background,p);

                                    // The Icon that is displayed in the underlying view
                                    icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_edit_mode);

                                    // Tells the drawer where to put the icon in the underlying view
                                    RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);

                                    // If the user has displaced the view more than X do this
                                    if (dX > 200){
                                        // Draws the icon in the underlying view
                                        c.drawBitmap(icon,null,icon_dest,p);
                                    }
                                }

                                // If the displacement of the view is less than 0 (Going left)
                                else if (dX < 0){

                                    // Sets the background color for the underlying view
                                    p.setColor(Color.parseColor("#6C27C8"));

                                    // Creates a rectangle given the dimensions of the OpenPosition view above it
                                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());

                                    // Draws said rectangle
                                    c.drawRect(background,p);

                                    // The Icon that is displayed in the underlying view
                                    icon = BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_input_delete);

                                    // Tells the drawer where to put the icon in the underlying view
                                    RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);

                                    // If the user has displaced the view more than X do this
                                    if (dX < -200) {
                                        // Draws the icon in the underlying view
                                        c.drawBitmap(icon,null,icon_dest,p);

                                    }
                                }
                            }
                            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        }
                    };
            return simpleItemTouchCallback;
        }

        // Moves OpenPosition
        private void moveItem(int oldPos, int newPos){
            // Get the item that is clicked on and its data based on its old position
            OpenPosition item = adapter.getSpecificData(oldPos);

            // Remove the data for that OpenPosition from it's old position
            adapter.removeSpecificData(oldPos);

            // Add the items information to the new position in the dataList
            adapter.addSpecificData(newPos, item);

            // Tell the adapter that an item needs to be moved, giving it the old and new position in the dataList so that it can sync it in the RecyclerView
            adapter.notifyItemMoved(oldPos, newPos);
        }

        // Deletes OpenPosition
        private void deleteItem(final int position){
            // Removes item in the data list
            adapter.removeSpecificData(position);

            // Tells the adapter the and item needs to be removed. It is given the position of the item on the data list so that it can sync it with the RecyclerView
            adapter.notifyItemRemoved(position);
        }
    }

    // Fragment to hold live orders
    public static class ClosedOrdersFragment extends android.support.v4.app.Fragment {

        // Fields
        LinearLayout ltDetails;
        TextView tvDetailView;
        ImageButton ibDotOptions;
        RecyclerView rvPostions;

        public ClosedOrdersFragment(){
            super();
            // Empty Constructor
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            // The main view
            View mainView = inflater.inflate(R.layout.fragment_closed_positions, container, false);

            // Init Views
            ltDetails = (LinearLayout) mainView.findViewById(R.id.ltDetails);
            tvDetailView = (TextView) mainView.findViewById(R.id.tvDetailView);
            ibDotOptions = (ImageButton) mainView.findViewById(R.id.ibDotOptinos);

            // TODO: Instantiate Recycler View, which will need a recycler view adapter as well as it's own layout. Refer to Ascendapp recycler view for guidance
            return mainView;

        }
    }

//// API Calls ////


}
