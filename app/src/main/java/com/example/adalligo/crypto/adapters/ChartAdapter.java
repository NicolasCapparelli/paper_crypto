package com.example.adalligo.crypto.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.RecyclerView;

import com.example.adalligo.crypto.custom.HomeChartFragment;
import com.example.adalligo.crypto.custom.HomeChartFragment2;

/**
 * Created by andrew.kenreich on 8/14/2017.
 */

public class ChartAdapter extends FragmentPagerAdapter {

    public ChartAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    //returns fragment position for the chartadapter. - needed to know what swipe fragment you are on.
    public Fragment getItem(int pos) {

        Fragment f = null;

        if(pos == 0){
            //calls method to get fragment custom/homechartfragment
            f = new HomeChartFragment();
        }else if(pos == 1)
        {
            //calls method to get fragment custom/homechartfragment2
            f = new HomeChartFragment2();
        }
        return f;
    }

    //Total number of fragments - add to this number to get more fragments later
    @Override
    public int getCount() {
        return 2;
    }


    //Return Page Title depending on position - this will be for later.
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "chart1";
        }else if(position == 1)
        {
            return "chart2";
        }
        return super.getPageTitle(position);

    }
}
