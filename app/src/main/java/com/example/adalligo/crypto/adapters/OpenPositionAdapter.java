package com.example.adalligo.crypto.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.custom.OpenPosition;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.recyclerview.R.styleable.RecyclerView;

/**
 * Created by Nico on 7/3/2017.
 */

public class OpenPositionAdapter extends RecyclerView.Adapter<OpenPositionAdapter.PositionViewHolder> {

    // Fields
    private ArrayList<OpenPosition> completeDataList;
    private ArrayList<OpenPosition> viewableDataList;
    private LayoutInflater inflater;
    private ItemClickCallback itemClickCallback;
    private OpenPositionAdapter thisAdapter;

    // Adapter Constructor
    public OpenPositionAdapter(Context context){
        this.inflater = LayoutInflater.from(context);
        this.completeDataList = new ArrayList<>();
        this.viewableDataList = completeDataList;
        this.thisAdapter = this;

    }

    // Interface that will give a callback once the view is clicked, which will allow us to tell which view in the list was clicked
    public interface ItemClickCallback{
        void onItemClick(int p);
    }

    // Setter for the interface
    public void setItemClickCallback(final ItemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }
    // Inflates the view
    @Override
    public PositionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // The view that needs to be inflated
        View mainView = inflater.inflate(R.layout.recview_open_position, parent, false);

        // Return new Position view holder
        return new PositionViewHolder(mainView);
    }

    // Updates price viewableDataList for everything in the viewableDataList list

    // Binds (Matches up) the viewableDataList class with the view
    @Override
    public void onBindViewHolder(final PositionViewHolder holder, int position) {
        // Getting the Data class from the viewableDataList ArrayList to be used for X view holder
        OpenPosition opData = this.viewableDataList.get(position);

        // Calculates initial and current values
        opData.calculateValues();
        // Updates the data inside the position object
        opData.updateChanges();

        // Setting the values for the views inside the view holder with the viewableDataList from the above openPositionDataClass that corresponds to this view's position
        holder.ivCoinLogo.setImageResource(opData.getCoinLogoID());
        holder.ivMovementIndicator.setImageResource(opData.getMovementIndicatorID());
        holder.tvBuyPrice.setText(opData.getInitialValueString());
        holder.tvCurrentPrice.setText(opData.getCurrentValueString());

        // Setting the values for the extended view
        holder.tvOrderSizeTitle.setText(opData.getCoinType());
        holder.tvOrderSizeAmount.setText(opData.getOrderSizeString());
        holder.tvDollarChange.setText(opData.getDollarChangeString());
        holder.tvPercentChange.setText(opData.getPercentChangeString());

        if (opData.isProfitable()){
            holder.tvDollarChange.setTextColor(holder.getViewContext().getColor(R.color.upwardGreen));
            holder.tvPercentChange.setTextColor(holder.getViewContext().getColor(R.color.upwardGreen));
        } else {
            holder.tvDollarChange.setTextColor(holder.getViewContext().getColor(R.color.downwardRed));
            holder.tvPercentChange.setTextColor(holder.getViewContext().getColor(R.color.downwardRed));
        }

        if (opData.getIsExtendedShowing()){

            // Set the views background to selected
            holder.ltContainer.setBackground(holder.getViewContext().getDrawable(R.drawable.selected_background));

            // Make the view visible
            holder.ltExtendedView.setVisibility(View.VISIBLE);
        }

        else {

            // Remove selected background

            // Make the view invisible
            holder.ltExtendedView.setVisibility(View.GONE);
        }

        // onClick Listener
        holder.ltContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickCallback.onItemClick(holder.getAdapterPosition());
            }
        });

    }

    // This method is used for updating the view partially
    @Override
    public void onBindViewHolder(PositionViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);

        // Full view update
        if (payloads.isEmpty()){
            holder.tvCurrentPrice.setText(viewableDataList.get(position).getCurrentValueString());
        }

        // Partial view update
        else {
            // Get the bundle from the payload list
            Bundle updateBundle = (Bundle) payloads.get(0);

            // coinType
            String coinType = updateBundle.getString("coinType");

            // Get string from the bundle
            String price = updateBundle.getString("newPrice");

            if (coinType.equals(viewableDataList.get(position).getCoinType())){
                // Update view
                this.viewableDataList.get(position).setCurrentPrice(Double.valueOf(price));
            }
        }
    }

    // Tells the Adapter the amount of total views
    @Override
    public int getItemCount() {
        return this.viewableDataList.size();
    }

    // Makes the complete data list viewable | NOTE: When using this method be sure to notifyDataSetChanged afterwards
    public void setCompleteDataListAsViewable(){
        // Set the complete list as viewable
        this.viewableDataList = this.completeDataList;
    }

    // Filters the viewable list by the coin passed | NOTE: When using this method be sure to notifyDataSetChanged afterwards
    public void filterViewableListByCoin(String coin){

        // New list that will contain only OpenPositions of the coin passed in
        ArrayList<OpenPosition> filteredList = new ArrayList<>();

        // For every OpenPosition
        for (OpenPosition iteratedPosition : this.completeDataList) {

            // If the open position coin type matches the one passed in
            if (iteratedPosition.getCoinType().equals(coin)){

                // Add the position object to the filtered list
                filteredList.add(iteratedPosition);
            }
        }

        // Make the viewable data list the filtered list
        this.viewableDataList = filteredList;
    }

    // Sets new viewableDataList
    public void setCompleteDataList(ArrayList<OpenPosition> newData){
        this.completeDataList = newData;
    }

    // Changes visibility of extended view
    public void setExtendedViewVisibility(int position, boolean bool){
        this.completeDataList.get(position).setIsExtendedShowing(bool);
    }


    // Get viewable dataList
    public ArrayList<OpenPosition> getViewableDataList(){
        return this.viewableDataList;
    }

    // Get viewableDataList
    public OpenPosition getSpecificData(int position){
        return this.viewableDataList.get(position);
    }

    // Add viewableDataList
    public void addSpecificData(int position, OpenPosition newData){
        this.viewableDataList.add(position, newData);
    }

    // Removes an OpenPosition object in the given position of the viewableDataList list
    public void removeSpecificData(int position){
        this.viewableDataList.remove(position);
    }

    // Returns the position of OpenPosition objects that the coinType in the viewable data list
    public ArrayList<Integer> getPositionsWithCoinType(String coinType){

        // ArrayList to be returned
        ArrayList<Integer> positionValues = new ArrayList<>();

        ArrayList<OpenPosition> data = this.viewableDataList;

        // Iterate through viewable data list
        for (int i = 0; i < data.size(); i++){

            // If the OpenPosition we are iterating through has the coinType given in the argument
            if (data.get(i).getCoinType().equals(coinType)){
                // Add that position to the ArrayList to be returned
                positionValues.add(i);
            }
        }

        // Return the ArrayList of values
        return positionValues;
    }

    // View Holder for our position view
    public class PositionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        // Fields
        RelativeLayout ltContainer;
        LinearLayout ltPositionView;
        ImageView ivCoinLogo;
        TextView tvBuyPrice;
        ImageView ivArrow;
        TextView tvCurrentPrice;
        ImageView ivMovementIndicator;

        // Extended View Fields
        RelativeLayout ltExtendedView;
        TextView tvOrderSizeTitle;
        TextView tvOrderSizeAmount;
        TextView tvOrderTimeTitle;
        TextView tvDollarChange;
        TextView tvPercentChange;


        public PositionViewHolder(View itemView) {
            super(itemView);

            // Init Views
            ltContainer = (RelativeLayout) itemView.findViewById(R.id.ltContainer);
            ltExtendedView = (RelativeLayout) itemView.findViewById(R.id.ltExtendedView);
            ltPositionView = (LinearLayout) itemView.findViewById(R.id.ltPositionView);
            ivCoinLogo = (ImageView) itemView.findViewById(R.id.ivCoinLogo);
            tvBuyPrice = (TextView) itemView.findViewById(R.id.tvBuyPrice);
            ivArrow = (ImageView) itemView.findViewById(R.id.ivArrow);
            tvCurrentPrice = (TextView) itemView.findViewById(R.id.tvCurrentPrice);
            ivMovementIndicator = (ImageView) itemView.findViewById(R.id.ivMovementIndicator);

            // Init Extended Views
            tvOrderSizeTitle = (TextView) itemView.findViewById(R.id.tvOrderSizeTitle);
            tvOrderSizeAmount = (TextView) itemView.findViewById(R.id.tvOrderSizeAmount);
            tvOrderTimeTitle = (TextView) itemView.findViewById(R.id.tvOrderTimeTitle);
            tvDollarChange = (TextView) itemView.findViewById(R.id.tvDollarChange);
            tvPercentChange = (TextView) itemView.findViewById(R.id.tvPercentChange);

        }

        // Get context
        public Context  getViewContext(){
            return itemView.getContext();
        }

        // On click listener
        @Override
        public void onClick(View v) {
            // If the view that was clicked on was the entire Position View
            if (v.getId() == R.id.ltContainer) {
                // Gets the position of the clicked item in the list supplied to the adapter
                itemClickCallback.onItemClick(this.getAdapterPosition());

            }

        }
    }
}
