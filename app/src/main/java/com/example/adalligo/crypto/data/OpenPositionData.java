package com.example.adalligo.crypto.data;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.custom.OpenPosition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Response;




/**
 * Created by Nico on 7/4/2017.
 *
 */

public class OpenPositionData {

    // Parses the json response and returns a new OpenPositionObject
    public static ArrayList<OpenPosition> parseResponse(Response response) throws IOException, JSONException {

        // Data ArrayList
        ArrayList<OpenPosition> data = new ArrayList<>();


        // Turning response into a string and then building a JSON object with it
        String resp = response.body().string();

        // Instantiating the JSONObject
        JSONObject respJson = null;

        // If success
        if (response.code() == 200){

            //Turning the response into a JSONObject
            respJson = new JSONObject(resp);

            // Taking the JSONArray that holds the position information out of the response
            JSONArray respArray = respJson.getJSONArray("openpositions");

            // Creates OpenPosition Objects with the data and stores them in the array
            for (int i = 0; i < respArray.length(); i++){

                // JSON of inner
                JSONObject openPositionJSON = respArray.getJSONObject(i);

                // Data values from the openPositionJSON
                String orderNumber = openPositionJSON.getString("ordernum");
                String dateTime = openPositionJSON.getString("time");
                String coinType = openPositionJSON.getString("coin");
                double orderSize = openPositionJSON.getDouble("positionsize");
                double buyInPrice = openPositionJSON.getDouble("coincost");

                // Creating an OpenPosition object with the above values
                OpenPosition openPosition = new OpenPosition(orderNumber, dateTime, coinType, orderSize, buyInPrice);

                // Adding said object to our data list
                data.add(openPosition);
            }
        }

        return data;
    }

}
