package com.example.adalligo.crypto.custom;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.adalligo.crypto.R;

import org.w3c.dom.Text;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

/**
 * Created by Nico on 6/27/2017.
 */

public class FingerprintHandler {

    // Fields
    private Context context;
    private KeyguardManager keyguardManager;
    private android.hardware.fingerprint.FingerprintManager fingMang;
    private android.os.CancellationSignal cancellationSignal;
    private FingerprintManager.AuthenticationCallback authenticationCallback;
    private OnAuthenticationSuccessListener onSuccessListener;
    private OnAuthenticationFailureListener onFailedListener;
    private TextView tvInstructions;


    /**
     * Interfaces are methods that can be implemented if the user wants to use the class,
     * in this case our two interfaces handle what happen when authentication succeeds or fails
     *
     **/

    // On auth success
    public interface OnAuthenticationSuccessListener {
        void onAuthSucceeded() throws Exception;
    }

    // On auth Failure
    public interface OnAuthenticationFailureListener {
        void onAuthFailed();
    }

    // Instantiates the success listener interface
    public void setOnAuthenticationSucceededListener (OnAuthenticationSuccessListener listener){
        onSuccessListener = listener;
    }

    // Instantiates the failure listener interface
    public void setOnAuthenticationFailedListener(OnAuthenticationFailureListener listener) {
        onFailedListener = listener;
    }

    // Constructor
    // @Params: context, ID int of Text View (i.e. R.id.etInstructions)
    public FingerprintHandler (final Context activity, int viewID) {
        // Context
        context = activity;
        // Instantiates fingerprint manager
        fingMang = (FingerprintManager) activity.getSystemService(FINGERPRINT_SERVICE);
        // The View to change text with
        tvInstructions = (TextView) ((Activity)context).findViewById(viewID);
        // Instantiates Keyguard Manager
        keyguardManager = (KeyguardManager) activity.getSystemService(KEYGUARD_SERVICE);
        // Cancellation signal
        cancellationSignal = new android.os.CancellationSignal();

        // The callback from the fingerprint sensor, telling us what happened when the user tried to authenticate
        authenticationCallback = new FingerprintManager.AuthenticationCallback() {

            // If there was an authentication error
            @Override
            public void onAuthenticationError(int errorCode, CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                // Set the text of the view passed in to the string telling the user how to fix the issue
                tvInstructions.setText(errString);
            }

            // If there was a physical error with the sensor, such as "Sensor Dirty", a help code and a readable string telling the user how to fix the issue
            @Override
            public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                super.onAuthenticationHelp(helpCode, helpString);
                // Set the text of the view passed in to the string telling the user how to fix the issue
                tvInstructions.setText(helpString);
            }

            // If the authentication is successful
            @Override
            public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                // Fire the interface method
                try {
                    if( onSuccessListener != null ){
                        onSuccessListener.onAuthSucceeded();
                    }

                    else {
                        Toast.makeText(context, "No Listener", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.d(">>>>>>>>>>>>>>>", e.toString());
                }

            }

            // If the authentication failed
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                // Fire the interface method for onFailedListener
                onFailedListener.onAuthFailed();
            }
        };
    }


////  Methods ////

    // Checks various conditions to see if a fingerprint can be used, returns a status code
    public int isFingerprintUsableCode (){

        // The response
        int response = 900;

        // If the user has not granted fingerprint permissions
        if ( ContextCompat.checkSelfPermission( context, Manifest.permission.USE_FINGERPRINT ) != PackageManager.PERMISSION_GRANTED ) {
            response = 901;
        }

        // If the user does not have the hardware needed for fingerprint log in
        if (!fingMang.isHardwareDetected()){
            response = 902;
        }

        // If the user does not have any fingerprints enrolled
        if (!fingMang.hasEnrolledFingerprints()){
            response = 903;
        }

        // If the user does not have a way of securely unlocking their phone (Fingerprint, Password, Pin, Pattern)
        if (!keyguardManager.isKeyguardSecure()) {
            response = 904;
        }

        return response;
    }

    // Start listening for a fingerprint response
    public void startListening(){
        // If the user has not granted fingerprint permissions
        if ( ContextCompat.checkSelfPermission( context, Manifest.permission.USE_FINGERPRINT ) == PackageManager.PERMISSION_GRANTED ) {
            try {
                // Start listening for fingerprint
                // @Params: CryptoObject (null), cancellationSignal, Flags, callback, Handler(null)
                // CryptoObject: Only used if you're using Android Key Manager
                // Flags: Optional Flags, should be 0 according to documentation
                // Handler: An option to handle callback events, not needed since that's the point of this entire class
                fingMang.authenticate(null, cancellationSignal, 0, authenticationCallback, null);
            }
            catch (Exception e) {
                Log.d(">>>>>>>>>>>>>>>", e.toString());
                // Tell the user there was an unknown error
                Toast.makeText(this.context, "An unknown error occurred gathering your fingerprint", Toast.LENGTH_SHORT).show();
            }
        }
        // Else
        else {
            Log.d(">>>>>>>>>>>>", "Permission not granted:" + ContextCompat.checkSelfPermission( context, Manifest.permission.USE_FINGERPRINT) + "/" + PackageManager.PERMISSION_GRANTED);

        }
    }

    // Stops listening for a fingerprint response
    public void stopListening(){
        if ( ContextCompat.checkSelfPermission( context, Manifest.permission.USE_FINGERPRINT ) != PackageManager.PERMISSION_GRANTED ){

            try {
                // Fires the cancellation signal to stop listening for fingerprints
                cancellationSignal.cancel();
                cancellationSignal = null;
            } catch (Exception e){
                Log.d(">>>>>>>>>>>>>>>", e.toString());
            }
        }
    }
}
