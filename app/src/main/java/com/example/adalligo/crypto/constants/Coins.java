package com.example.adalligo.crypto.constants;

import com.example.adalligo.crypto.R;

/**
 * Created by Nico on 7/6/2017.
 */

public class Coins {

    // Constants
    public static final int BTC = R.drawable.ic_ad_btc_logo;
    public static final int ETH = R.drawable.ic_ad_eth_logo;
    public static final int LTC = R.drawable.ic_ad_ltc_logo;

}
