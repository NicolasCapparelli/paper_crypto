package com.example.adalligo.crypto.data;

import android.os.Bundle;
import android.util.Log;

import com.example.adalligo.crypto.api.AdalligoAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Nico on 7/24/2017.
 * Note: The purpose of this class is to reduce API price calls to the server.
 * Prices are Strings because this class does NOT do any calculations with them, and is to be used
 * for putting prices into views
 */

public class UpdatedCoinPriceData {


    // Fields
    private Bundle btcPrice;
    private Bundle ethPrice;
    private Bundle ltcPrice;
    private UpdatedCoinPriceData.onPriceUpdateListener priceUpdateListener;


    // Interface for BTC price change
    public interface onPriceUpdateListener{
        void onPriceChange();
    }

    // Setters for the interfaces
    public void setPriceChangeListener(UpdatedCoinPriceData.onPriceUpdateListener listener){
        priceUpdateListener = listener;
    }


    // Constructor
    public UpdatedCoinPriceData() {
        // Instantiating bundles
        btcPrice = new Bundle();
        ethPrice = new Bundle();
        ltcPrice = new Bundle();
    }


    // Methods
    public void asyncGetPrices() throws IOException, JSONException {

        Request request = AdalligoAPI.getAllCoinPrices();

        AdalligoAPI.CLIENT.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("UCPD FAILURE", "FAILURE: " + e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                // If request success
                if (response.code() == 200) {

                    // JSON Array where the price data will be stored
                    JSONArray priceArray;

                    try {
                        // Put actual response body in array
                        priceArray = new JSONArray(response.body().string());

                        // Getting the price values out of the array and putting them into the currencyPriceVariables
                        btcPrice.putDouble("currentPrice", new JSONObject(priceArray.get(0).toString()).getDouble("price"));
                        ethPrice.putDouble("currentPrice", new JSONObject(priceArray.get(1).toString()).getDouble("price"));
                        ltcPrice.putDouble("currentPrice", new JSONObject(priceArray.get(2).toString()).getDouble("price"));

                        // TODO: Add open price in here


                    }

                    // If something goes wrong with the parsing, SHOULD NOT HAPPEN UNLESS THERE IS A SERVER SIDE CHANGE
                    catch (JSONException e) {

                        // Set all coin prices to -1 denoting an error
                        btcPrice.putDouble("currentPrice", -1);
                        ethPrice.putDouble("currentPrice", -1);
                        ltcPrice.putDouble("currentPrice", -1);

                        e.printStackTrace();
                    }
                }

                // If request is not successful,
                else {

                    // Set all coin prices to -1 denoting an error
                    btcPrice.putDouble("currentPrice", -1);
                    ethPrice.putDouble("currentPrice", -1);
                    ltcPrice.putDouble("currentPrice", -1);

                }

                // Fire interface method after changes have been made
                priceUpdateListener.onPriceChange();

            }
        });
    }

    //
    public void get24HourPrices() throws IOException, JSONException {

        final Request request = AdalligoAPI.get24PriceData();

        AdalligoAPI.CLIENT.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("UCPD FAILURE", "FAILURE: " + e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() == 200){

                    // JSON Array where the price data will be stored
                    JSONArray priceArray;

                    try {

                        priceArray = new JSONArray(response.body().string());

                        for (int i = 0; i < priceArray.length(); i++){

                            JSONObject currObj = priceArray.getJSONObject(i);

                            if (currObj.getString("currency").equals("btc")){
                                btcPrice.putDouble("openPrice", currObj.getDouble("open"));
                            }

                            else if (currObj.getString("currency").equals("eth")){
                                ethPrice.putDouble("openPrice", currObj.getDouble("open"));
                            }

                            else if (currObj.getString("currency").equals("ltc")){
                                ltcPrice.putDouble("openPrice", currObj.getDouble("open"));
                            }
                        }
                    }

                    // If there is some exception
                    catch (Exception e){

                        // Set all coin prices to -1 denoting an error
                        btcPrice.putDouble("openPrice", -1);
                        ethPrice.putDouble("openPrice", -1);
                        ltcPrice.putDouble("openPrice", -1);
                    }
                }

                else {

                    btcPrice.putDouble("openPrice", -1);
                    ethPrice.putDouble("openPrice", -1);
                    ltcPrice.putDouble("openPrice", -1);

                }

                try {
                    asyncGetPrices();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    // Returns coin data given the coinType
    public Bundle getCoinData(String coin){
        switch (coin){
            case "BTC":
                return btcPrice;
            case "ETH":
                return ethPrice;
            case "LTC":
                return ltcPrice;
            default:
                return null;
        }
    }
}
