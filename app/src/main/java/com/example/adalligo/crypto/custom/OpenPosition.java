package com.example.adalligo.crypto.custom;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.constants.Coins;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Nico on 7/3/2017.
 */

public class OpenPosition {

    // Value Fields used for calculations

    private String orderNumber;
    private String dateTime;
    private String coinType;
    private String orderInfo;

    private double orderSize;
    private double buyInPrice;

    private double initialValue;
    private double currentValue;
    private double currentPrice;
    private double dollarChange;

    private boolean isExtendedShowing;
    private double percentChange;
    // Used to format doubles to two decimals
    private NumberFormat decimalFormat = new DecimalFormat("#0.00");

    // Image Fields
    private int coinLogoID;
    private int movementIndicatorID;

    // Constructor
    public OpenPosition(String orderNumber, String dateTime, String coinType, double orderSize, double buyInPrice){
        // Setting the local values with the ones passed in
        this.orderNumber = orderNumber;
        this.dateTime = dateTime;
        this.coinType = coinType;
        this.orderSize = orderSize;
        this.buyInPrice = buyInPrice;
        this.isExtendedShowing = false;

        // This value needs to be updated via the setter from the class that is using it
        this.currentPrice = 0;

        this.dollarChange = this.currentValue - this.initialValue;
        this.percentChange = this.dollarChange / this.initialValue;
        setCoinLogoID();
        setMovementIndicatorID();


    }

    // Constructor For Positions with orderInformation (Position name)
    public OpenPosition(String orderInfo, String orderNumber, String dateTime, String coinType, double orderSize, double buyInPrice){
        // Setting the local values with the ones passed in
        this.orderInfo = orderInfo;
        this.orderNumber = orderNumber;
        this.dateTime = dateTime;
        this.coinType = coinType;
        this.orderSize = orderSize;
        this.buyInPrice = buyInPrice;
        this.isExtendedShowing = false;

        // This value needs to be updated via the setter from the class that is using it
        this.currentPrice = 0;

        this.dollarChange = this.currentPrice - this.buyInPrice;
        this.percentChange = (this.dollarChange / this.buyInPrice) * 100;
        setCoinLogoID();
        setMovementIndicatorID();

    }

// Methods //

    // Calculates initial value
    public void calculateValues() {

        // Calculates initial value
        this.initialValue = this.orderSize * this.buyInPrice;

        // Calculates current value
        this.currentValue = this.orderSize * this.currentPrice;
    }

    // Checks if the trade is profitable
    public boolean isProfitable(){

        // If profitable
        if (buyInPrice < currentPrice){
            return true;
        }

        // Else
        else {
            return false;
        }
    }


    // Updates the data in the class
    public void updateData(double buyInPrice, double currentPrice){
        this.buyInPrice = buyInPrice;
        this.currentPrice = currentPrice;
        updateChanges();
    }

    // Updates the change values and re-sets the movement indicator
    public void updateChanges(){
        dollarChange = currentValue - initialValue;
        percentChange = (dollarChange / initialValue) * 100;
        setMovementIndicatorID();
    }

    // Changes the movement indicator
    private void setMovementIndicatorID(){
        // If change is positive
        if (this.dollarChange > 0) {
            movementIndicatorID = R.drawable.ic_ad_normal_upwards_trend;
        }

        // If change is negative
        else {
            movementIndicatorID = R.drawable.ic_ad_normal_downwards_trend;
        }
    }

    // Set the coin logo
    private void setCoinLogoID(){

        if (this.coinType.equals("BTC")){
            this.coinLogoID = Coins.BTC;
        }

        else if (this.coinType.equals("ETH")){
            this.coinLogoID = Coins.ETH;
        }

        else if (this.coinType.equals("LTC")){
            this.coinLogoID = Coins.LTC;
        }

        else {
            this.coinLogoID = 0;
            // TODO: Error handling here somehow
        }

    }


// Getters & Setters //

    // Returns the coinType
    public String getCoinType(){
        return coinType;
    }

    // coinLogoID
    public int getCoinLogoID() {
        return coinLogoID;
    }

    // Returns buyInPrice as string
    public String getInitialValueString(){
        return "$" + String.valueOf(initialValue);
    }

    // Returns currentPrice as string
    public String getCurrentValueString(){
        return "$" + String.valueOf(currentValue);
    }

    // movementIndicatorID
    public int getMovementIndicatorID(){
        return movementIndicatorID;
    }

    // Order Size
    public String getOrderSizeString(){
        return String.valueOf(this.orderSize);
    }

    // Returns dollar change as string
    public String getDollarChangeString(){

        // If the position is profitable
        if (isProfitable()){

            // Returns +$X.XX
            return "+$" + decimalFormat.format(dollarChange);
        }

        // If it's not
        else {

            // Absolute value of of dollarChange
            double returnValue = Math.abs(dollarChange);
            // Returns -$X.XX
            return "-$" + decimalFormat.format(returnValue);
        }


    }

    // Returns percent change as string
    public String getPercentChangeString(){
        return decimalFormat.format(percentChange) + "%";
    }

    // Order Number
    public String getOrderNumber(){
        return this.orderNumber;
    }

    // Date & Time of purchase
    public String getDateTime(){
        return String.valueOf(this.dateTime);
    }

    public boolean getIsExtendedShowing() {
        return isExtendedShowing;
    }





    // Sets a new current price
    public void setCurrentPrice(double newPrice){
        this.currentPrice = newPrice;
    }

    // Sets visibility of extended view
    public void setIsExtendedShowing(boolean bool){
        this.isExtendedShowing = bool;
    }

}
