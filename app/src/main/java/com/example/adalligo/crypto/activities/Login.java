package com.example.adalligo.crypto.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adalligo.crypto.R;
import com.example.adalligo.crypto.api.AdalligoAPI;
import com.example.adalligo.crypto.helper.Validation;

import org.json.JSONException;

public class Login extends AppCompatActivity {


    private static Context thisActivity;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor prefEditor;
    private static android.app.FragmentManager fragManager;
    private static String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Context
        thisActivity = this;

        // Fragment Manager
        fragManager = getFragmentManager();

        // Preferences
        preferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);

        // Preference Editor
        prefEditor = preferences.edit();


        // Fragment
        MainFragment mainFragment = new MainFragment();
        // Fragment Manager
        fragManager = getFragmentManager();
        // Makes the MainFragment visible
        fragManager.beginTransaction().add(R.id.fragmentFrame,mainFragment,mainFragment.TAG).commit();


    }


    // Main Fragment
    public static class MainFragment extends android.app.Fragment {

        // Fields
        ImageView ivLogo;
        EditText etEmail;
        EditText etPwd;
        CheckBox cbRememberMe;
        Button btLogin;
        TextView tvForgotPwd;

        // Tag
        public static final String TAG = MainFragment.class.getSimpleName();

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View mainView = inflater.inflate(R.layout.login_main_fragment, container, false);

            // Initializing Views
            ivLogo = (ImageView) mainView.findViewById(R.id.ivLogo);
            etEmail = (EditText) mainView.findViewById(R.id.etEmail);
            etPwd = (EditText) mainView.findViewById(R.id.etPwd);
            cbRememberMe = (CheckBox) mainView.findViewById(R.id.cbRemember);
            btLogin = (Button) mainView.findViewById(R.id.btLogin);
            tvForgotPwd = (TextView) mainView.findViewById(R.id.tvForgotPwd);

            // If the user previously opted to remember credentials
            if (preferences.getBoolean("isRememberedLogin", false)){
                // Set the CheckBox to checked
                cbRememberMe.setChecked(true);
                // Set the credentials in their respective EditTexts
                etEmail.setText(preferences.getString("email", ""));
                etPwd.setText(preferences.getString("pwd", ""));
            }


            // Check Box Listeners
            cbRememberMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // If the checkbox is not checked and the user checks it
                    if (cbRememberMe.isChecked()){
                        prefEditor.putBoolean("isRememberedLogin", true);
                        prefEditor.commit();
                    }

                    // If the checkbox is checked and the user un-checks it
                    else {
                        prefEditor.putBoolean("isRememberedLogin", false);
                        prefEditor.commit();
                    }
                }
            });

            // Login button click listener
            btLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Get string values from Edit Texts
                    String email = etEmail.getText().toString();
                    String pwd = etPwd.getText().toString();

                    // Run login method with gathered values
                    try {
                        loginSequence(email, pwd);
                    } catch (Exception e) {
                        Toast.makeText(thisActivity, "Some err", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            // Forgot password view click listener to go to reset password
            tvForgotPwd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Fragment
                    PasswordResetPartOneFragment passFrag = new PasswordResetPartOneFragment();
                    // Change fragments and add the main fragment to back stack (handles what happens when the user presses the back button)
                    fragManager.beginTransaction().replace(R.id.fragmentFrame,passFrag,passFrag.TAG).addToBackStack(passFrag.TAG).commit();
                }
            });

            return mainView;
        }

        // Runs login sequence
        private void loginSequence(String email, String pwd) throws Exception {
            AdalligoAPI.asyncLogin(thisActivity, email, pwd);

        }

    }


    // Part one fragment
    public static class PasswordResetPartOneFragment extends android.app.Fragment {

        // Tag to identify the fragment
        public static final String TAG = PasswordResetPartOneFragment.class.getSimpleName();

        // Fields
        EditText etEmail;
        TextView tvInstructions;
        Button btResetPwd;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View mainView = inflater.inflate(R.layout.fragment_reset_one, container, false);

            // Init Views
            etEmail = (EditText) mainView.findViewById(R.id.etEmail);
            tvInstructions = (TextView) mainView.findViewById(R.id.tvInstructions);
            btResetPwd = (Button) mainView.findViewById(R.id.btResetPass);

            // Listener for reset password button
            btResetPwd.setOnClickListener(new View.OnClickListener() {
                @Override
                    public void onClick(View v) {

                    // Try reset password one API call
                    try {
                        // Run reset password one API call
                        AdalligoAPI.asyncResetPwdPartOne(thisActivity, R.id.tvInstructions, etEmail.getText().toString());
                        // Sets the correct email in the email variable that is part of the entire activity, not just the fragment, so we can use it in the same fragment
                        email = etEmail.getText().toString();
                    } catch (JSONException e) {
                        // network error
                        tvInstructions.setText("There was a network error");
                    }

                    // if the text view (which will be changed by the request method), if equal to the verificationCodeEmail string
                    if (tvInstructions.getText().toString().equals(getResources().getString(R.string.verificationCodeEmail))){
                        // Fragment
                        ResetPasswordFragmentTwo resPassFrag = new ResetPasswordFragmentTwo();
                        // Change fragments and add the main fragment to back stack (handles what happens when the user presses the back button)
                        fragManager.beginTransaction().replace(R.id.fragmentFrame,resPassFrag,resPassFrag.TAG).addToBackStack(resPassFrag.TAG).commit();
                    }
                }
            });
            return mainView;
        }
    }

    // Part two fragment
    public static class ResetPasswordFragmentTwo extends android.app.Fragment {

        // TAG
        public static final String TAG = ResetPasswordFragmentTwo.class.getSimpleName();

        // Fields
        EditText etEmail;
        EditText etCode;
        EditText etNewPwd;
        EditText etConfirmPwd;
        TextView tvIsPasswordSame;
        Button btResetPass;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View mainView = inflater.inflate(R.layout.fragment_resetpass, container, false);

            // tvPasswordConfirm id code
            final int tvPasswordConfirmCode = R.id.tvIsPasswordSame;

            // Init views
            etEmail = (EditText) mainView.findViewById(R.id.etEmail);
            etCode = (EditText) mainView.findViewById(R.id.etCode);
            etNewPwd = (EditText) mainView.findViewById(R.id.etNewPwd);
            etConfirmPwd = (EditText) mainView.findViewById(R.id.etConfirmPwd);
            tvIsPasswordSame = (TextView) mainView.findViewById(R.id.tvIsPasswordSame);
            btResetPass  = (Button) mainView.findViewById(R.id.btResetPass);



            // Listener for when the text is changed on the confirm password view
            etConfirmPwd.addTextChangedListener(new TextWatcher() {
                // Doesn't need to be used in this context
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                // When the text changes
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    // If password matches, tell the user that they match
                    if (doPwdsMatch()){
                        tvIsPasswordSame.setText(getResources().getString(R.string.passwordMatch));
                    }

                    // Else, tell the user that they do not match
                    else {
                        tvIsPasswordSame.setText(getResources().getString(R.string.passwordNoMatch));

                    }
                }

                // Doesn't need to be used in this context
                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            // Listener for reset pass button
            btResetPass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // If fields are validated and passwords match
                    if (validateFields() && doPwdsMatch()){
                        try {
                            AdalligoAPI.asyncResetPwdTwo(getContext(), R.id.tvIsPasswordSame, etCode.getText().toString(), email, etConfirmPwd.getText().toString());

                        }

                        // If the client was not able to connect to the server
                        catch (JSONException e) {
                            tvIsPasswordSame.setText("A network error ocurred, please try again later");
                        }
                    }
                }
            });

            // Returns view that needs to be put in the main activity
            return mainView;
        }

        // Methods

        // Checks all fields to make sure they are filled out
        private boolean validateFields(){

            // Bool to be set
            boolean isValidated;

            // If the email field is empty or doesn't match the pattern of an email string
            if (!Validation.validateEmail(etEmail.getText().toString())){
                Toast.makeText(getContext(), "Invalid Email Address", Toast.LENGTH_LONG).show();
                isValidated = false;

            }

            // If the verification code field is left empty
            else if (!Validation.validateFields(etCode.getText().toString())){
                Toast.makeText(getContext(), "The Verification Code Field Cannot Be Left Empty", Toast.LENGTH_LONG).show();
                isValidated = false;

            }

            // If the new password field is left empty
            else if (!Validation.validateFields(etNewPwd.getText().toString())){
                Toast.makeText(getContext(), "The New Password Field Cannot Be Left Empty", Toast.LENGTH_LONG).show();
                isValidated = false;
            }

            // If the confirm password field is left empty
            else if (!Validation.validateFields(etConfirmPwd.getText().toString())){
                Toast.makeText(getContext(), "The Confirm Password Field Cannot Be Left Empty", Toast.LENGTH_LONG).show();
                isValidated = false;
            }

            // If everything is good
            else {
                isValidated = true;

            }

            // Return bool
            return isValidated;
        }

        // Check if the new password and confirm password match
        private boolean doPwdsMatch(){

            // Bool to be set
            boolean match;

            // If the new password and the confirm password fields match
            if (etNewPwd.getText().toString().equals(etConfirmPwd.getText().toString())){
                match = true;
            }

            // If they don't
            else {
                match = false;
            }

            // Return bool
            return match;
        }
    }
}
